/*
 * @file: gabasorts.h
 * @author: Gabriel Conrado
 * @brief: Library that contains some sorting and related utility functions
 * @2018
 *  
 */

// Constants
#define BUBBLE_SORT 0
#define INSERTION_SORT 1
#define SELECTION_SORT 2
#define MERGE_SORT 3
#define HEAP_SORT 4
#define QUICK_SORT 5
#define SHELL_SORT 6
#define COUNTING_SORT 7

// Global utility functions declarations
int *readArray(int *n);
int isSorted(int *array, int n);
void printArray(int *array, int n);

// Global sorting functions declarations
void bubbleSort(int *array, int n);
void insertionSort(int *array, int n);
void selectionSort(int *array, int n);
void mergeSort(int *array, int left, int right);
void heapSort(int *array, int n);
void quickSort(int *array, int low, int high);
void shellSort(int *array, int n);
void countingSort(int *array, int n, int range);