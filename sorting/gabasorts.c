/*
 * @file: gabasorts.c
 * @author: Gabriel Conrado
 * @brief: Implementation of some sorting and related utility functions
 * @2018
 *  
 */

#include <stdio.h>
#include <stdlib.h>

// Local functions declaration
void merge(int *array, int left, int middle, int right);
void heapify(int *array, int n, int root);
int partition(int *array, int low, int high);

// Local functions implementation
void merge(int *array, int left, int middle, int right){
    int x, y, z;
    
    // Side vectors
    int l = middle - left + 1;
    int r = right - middle;
    int L[l], R[r];

    // Create the side vectors
    for(x = 0; x < l; x++)
        L[x] = array[left + x];
    for(x = 0; x < r; x++)
        R[x] = array[middle + 1 + x];

    // Zeroing indices
    x = y = 0;
    z = left;

    // Merging the two sub arrays
    while(x < l && y < r){
        if(L[x] < R[y])
            array[z++] = L[x++];
        else
            array[z++] = R[y++];
    }

    // Copy the rest of elements if there is still any
    while(x < l)
        array[z++] = L[x++];
    
    while(y < r)
        array[z++] = R[y++];

}

void heapify(int *array, int n, int root){

    // Initialize the tree positions
    int max = root;
    int left = 2*root + 1;
    int right = 2*root + 2;

    // Check if any of the childs are greater than the parent node
    if (left < n && array[left] > array[max])
        max = left;
    
    if (right < n && array[right] > array[max])
        max = right;

    // If max is not the root anymore, swap the positions
    if (max != root){
        int aux = array[max];
        array[max] = array[root];
        array[root] = aux;
        heapify(array, n, max);
    }
    
}

int partition(int *array, int low, int high){

    // Set pivot and lesser element index
    int pivot = array[high];
    int x = (low - 1);

    // Put all lower elements than the pivot to the left of it
    for (int i = low; i <= high - 1; i++){
        if (array[i] <= pivot){            
            x++;
            int aux = array[x];
            array[x] = array[i];
            array[i] = aux;
        }
    }

    // Put the pivot in its right position
    int aux = array[x+1];
    array[x+1] = array[high];
    array[high] = aux;

    return (x + 1);
}

// Utility functions implementation
int isSorted(int *array, int n){
    int x;
    for (x = 0; x < n-1; x++){
        if (array[x] > array[x+1])
            return 0;
    }
    return 1;
}

int *readArray(int *n){
    int x;
    scanf("%d", n);    
    int *array = (int *) malloc (*n * sizeof(int));
    for (x = 0; x < *n; x++){
        scanf("%d", (array + x));
    }
    return array;
}

void printArray(int *array, int n){
    int x;
    for (x = 0; x < n; x++)
        printf("%d ", array[x]);
    printf("\n");
}

// Sorting functions implementation
void bubbleSort(int *array, int n){
    int x, y;
    for (x = 0; x < n - 1; x++){
        for (y = 0; y < n - 1; y++){
            if (array[y] > array[y+1]){
                int aux = array[y];
                array[y] = array[y+1];
                array[y+1] = aux;
            }
        }
    }
}

void insertionSort(int *array, int n){
    int a, x, y;
    for (x = 1; x < n; x++){
        // Selected element
        a = array[x];
        // Moving index
        y = x - 1;
        // Shift elements at the left of selected element until find its place
        while ((y >= 0) && (array[y] > a)){
            array[y+1] = array[y];
            y--;
        }
        array[y+1] = a;
    }
}

void selectionSort(int *array, int n){
    int a, x, y, aux;
    for (x = 0; x < n-1; x++){
        // Minumum element index
        a = x;
        for(y = x+1; y < n; y++){
            // Update the minimum index if necessary
            if(array[y] < array[a])
                a = y;
        }
        // Move the minimum to the first position if it is not already
        if(array[x] != array[a]){
            aux = array[x];
            array[x] = array[a];
            array[a] = aux;
        }
    }
}

void mergeSort(int *array, int left, int right){

    // Stop condition
    if (left < right){
        int middle = (right + left) / 2;

        // Call the merge sort for the side vectors
        mergeSort(array, left, middle);
        mergeSort(array, middle + 1, right);
        merge(array, left, middle, right);
    }
}

void heapSort(int *array, int n){

    // Build the heap from the array
    for (int i = (n - 2) / 2; i >= 0; --i)
        heapify(array, n, i);
    
    // Extract the elements from the heap one at a time
    for (int i = n - 1; i >= 0; i--){

        // Move current root to end
        int aux = array[i];
        array[i] = array[0];
        array[0] = aux;

        // Call heapify on the reduced heap
        heapify(array, i, 0);

    }
}

void quickSort(int *array, int low, int high){

    if (low < high){
        // Pivot in the right place
        int pi = partition(array, low, high);

        // Sort the elements before and after partition
        quickSort(array, low, pi - 1);
        quickSort(array, pi+1, high);
    }
}

void shellSort(int *array, int n){

    // Initialize with a big window, then reduces it until it becomes one
    for (int window = n/2; window > 0; window /=2){
        // Apply windowed insertion sort
        for (int i = window; i < n; i++){
            int aux = array[i];
            // Find right location to selected element
            int y;
            for (y = i; y >= window && array[y - window] > aux; y -= window)
                array[y] = array[y - window];
            // Put the element into its right position
            array[y] = aux;
        }
    }
}

// THE INPUT ARRAY MUST HAVE VALUES GREATER THAN ZERO AND LESSER THAN RANGE 
void countingSort(int *array, int n, int range){

    // Build the zeroed reference array
    int *reference = (int *) calloc(range + 1, sizeof(int));

    // Go through the array and add a value into the reference vector at the
    // right position
    for (int i = 0; i < n; i++)
        reference[array[i]]++;
    
    // The output array index
    int y = 0;

    // Rebuild the output
    for (int i = 0; i < range; i++){
        while(reference[i] > 0){
            array[y++] = i;
            reference[i]--;
        }
    }

    free(reference);
}