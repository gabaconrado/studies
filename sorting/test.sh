#!/bin/bash
ALGORITHMS_SIZE=7
ALGORITHMS=("BUBBLE" "INSERTION" "SELECTION" "MERGE" "HEAP" "QUICK" "SHELL" \
    "COUNTING")

echo "---Building the executable---"
make
echo "---Testing the algorithms---"
for i in $(seq 0 $ALGORITHMS_SIZE)
do
    echo "---Testing ${ALGORITHMS[i]}"
    for filename in inputs/input*
    do
        if ./gabasorts.gaba $i < $filename; then
            echo "Success";
        else
            echo "Failed";
        fi
    done
done
echo "---Cleaning---"
make clean