# Sorting Algorithms

## Introduction

Sorting an array is very common and useful in computer science. Sorting can solve many problems or
simplify the solution to others.

> Sort an array is to generate a permutation of that array that follows a specific general rule.

They can be classified by some aspects

### Comparison sorts

When an algorithm is based on comparing two values and then placing one left and one right, it is a
_comparison based_ sort.

### Stability

When an algorithm keeps the order of equal values in the final array, it is a _stable_ sort.

### Adaptability

When the initial state of the array affects the runtime, it is an _adaptable_ sort.

### Complexity

The space and time complexity in **O** notation.

## Bubble sort

Simplest sorting algorithm, for each element compares it with the whole array and swaps their
position if necessary.

## Insertion sort

Efficient for small or almost-sorted arrays. It takes the elements of the list one by one and puts
then into their final spots in each iteration.

## Selection sort

Similar to the insertion sort, it takes the minimum value of the array and puts it in the first
unsorted position of the list.

## Merge sort

Merge sort divides the array into single elements, then start merging then sequentially until the
final permutation is achieved.

## Heap sort

Heap sort uses a binary tree(called heap) to implement a very efficient **selection sort**. In each
iteration the algorithm puts the minimum(or maximum) value at the tree node, and then puts it into
its right final position in the array.

## Quicksort

The quicksort algorithm works chosing a pivot(intermediary element), and then all lesser elements
of the original array are placed before the pivot, and the greater elements after the pivot. Then
the sublists are recursivelly sorted.

## Shellsort

A variation of the insertion sort that moves more than one element per iteration. The complexity
is not improved too much by this but the code is relativelly simple and the memory usage is very
low.

## Counting sort

When the input is in a known set of elements, the algorithm counts the ocurrence of each possible
value and then outputs them sorted.

## Bucket sort

Variation of the counting sort that divides the input into known _buckets_, then all the buckets
are sorted individually and merged at the end.

## Radix sort

The radix sort sorts numbers by processing individual digits, the algorithm starts in the LSD(or MSB
) sort the list, and then go to the next digit until the list is sorted.

## Summary

| Algorithm | Best case O | Average case O | Worst case O | Memory O | Stable | Comparison based |
| --------- | ----------- | -------------- | ------------ | -------- | ------ | ---------------- |
|Bubble|n|n²|n²|1|Yes|Yes|
|Insertion|n|n²|n²|1|Yes|Yes|
|Selection|n²|n²|n²|1|No|Yes|
|Merge|nlogn|nlogn|nlogn|n|Yes|Yes|
|Heap|nlogn|nlogn|nlogn|1|No|Yes|
|Quick|nlogn|nlogn|n²|logn|No|Yes|
|Shell|nlogn|--|n^(4/3)|1|No|Yes|
|Counting|--|n+r|n+r|n+r|Yes|No|
|Bucket|--|n+r|n+r|n+r|Yes|No|
|Radix|--|n*k/d|n*k/d|n+2^d|Yes|No|
