/*
 * @file: main.c
 * @author: Gabriel Conrado
 * @brief: Main wrapper to demonstrate the "gabasorts" sorting functions library
 * @2018
 *  
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "gabasorts.h"

// The binary has to be called with the wanted sorting algorithm as argument
// check gabasorts.h to check the available ones
int main(int argc, char const *argv[]){
    int n;
    int *array;
    struct timespec timebegin, timeend;
    // This function reads an array in the following format:
    // First line: N = number of elements in the array
    // Second..Nnd line: E = array element(integer number)
    array = readArray(&n);
    clock_gettime(CLOCK_REALTIME, &timebegin);
    if(argc == 2){
        switch(atoi(argv[1])){
            case BUBBLE_SORT:
                bubbleSort(array, n);
                break;
            case INSERTION_SORT:
                insertionSort(array, n);
                break;
            case SELECTION_SORT:
                selectionSort(array, n);
                break;
            case MERGE_SORT:
                mergeSort(array, 0, n - 1);
                break;
            case HEAP_SORT:
                heapSort(array, n);
                break;
            case QUICK_SORT:
                quickSort(array, 0, n-1);
                break;
            case SHELL_SORT:
                shellSort(array, n);
                break;
            case COUNTING_SORT:
                countingSort(array, n, 99);
                break;
            default:
                break;
        }
    }
    clock_gettime(CLOCK_REALTIME, &timeend);
    double exec_time = (double) (timeend.tv_nsec - timebegin.tv_nsec);
    printf("Sort time = %.0f nanoseconds - ", exec_time);    
    int result = !isSorted(array, n);
    free(array);
    return result;
}
