#!/usr/bin/env python3

# Dota 2 Hero Randomer
# A basic CLI to help you pick Dota heroes!

# This file is the entrypoint of the system

import sys
from dota_randomer.types import Attribute
from dota_randomer.repository import SampleHeroRepository
from dota_randomer.api import SampleHeroApi


def main():
    # Gets the attribute argument
    attr_arg = None
    if len(sys.argv) == 2:
        attr_arg = sys.argv[1]

    # Validates the attribute argument
    match attr_arg:
        case "agi":
            attr = Attribute.AGI
        case "str":
            attr = Attribute.STR
        case "int":
            attr = Attribute.INT
        case _:
            attr = None

    # Creates the repository
    repository = SampleHeroRepository()

    # Creates the api
    api = SampleHeroApi(repository)

    hero = api.get_random_hero(attr=attr)

    print(f"Selected hero: {hero}")


if __name__ == "__main__":
    main()
