"""
Definition of common helper types for the Dota 2 hero randomer
"""

from enum import Enum


class Attribute(Enum):
    """
    Lists all the attributes a hero can be
    """

    STR = 1
    AGI = 2
    INT = 3


class Hero:
    """
    Class to represent a Dota 2 hero
    """

    def __init__(self, name, attr):
        """
        Initializes a new hero
        @params:
            - name: The hero name
            - attr: The hero attribute
        """
        self.name = name
        self.attribute = attr

    def __str__(self):
        """
        Human readable form of a hero
        """
        return f"Hero {self.name}: {self.attribute}"
