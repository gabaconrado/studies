from . import HeroApi
from ..repository import HeroRepository


class SampleHeroApi:
    """
    Sample implementation of the hero api interface.
    It returns the first hero of the given attribute (if sent) returned from
    the data source.
    """

    def __init__(self, repository):
        """
        Sample Hero Api constructor
        @params:
            repository: The hero repository
        """
        self.repository = repository

    def get_random_hero(self, attr=None):
        """
        Returns the first hero of the given attribute
        @params:
            - [Optional] attr: An attribute to filter the hero pool
        @returns:
            - hero: The random hero, or None if there is no hero in the pool
        """
        all_heroes = self.repository.get_heroes()
        if len(all_heroes) == 0:
            return None
        if not attr:
            return all_heroes[0]
        for hero in all_heroes:
            if hero.attribute == attr:
                return hero
        return None
