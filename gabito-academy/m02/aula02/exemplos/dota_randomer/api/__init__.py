from .interface import HeroApi
from .sample_api import SampleHeroApi

__all__ = [HeroApi, SampleHeroApi]
