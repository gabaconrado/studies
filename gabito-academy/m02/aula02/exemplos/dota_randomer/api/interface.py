class HeroApi:
    """
    Defines the public methods to manage heroes
    """

    def get_random_hero(self, attr=None):
        """
        Gets a random hero from the pool.
        @params:
            - [Optional] attr: An attribute to filter the hero pool
        @returns:
            - hero: The random hero
        """
        raise NotImplementedError("Cannot call method from the interface")
