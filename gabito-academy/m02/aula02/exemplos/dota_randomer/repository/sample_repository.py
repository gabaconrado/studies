from . import HeroRepository
from ..types import Hero, Attribute


class SampleHeroRepository(HeroRepository):
    """
    Sample implementation of the hero repository interface.
    It contains a pre-defined list of heroes (Meepo, Pudge and Techies)
    """

    hero_list = [
        Hero("Meepo", Attribute.AGI),
        Hero("Pudge", Attribute.STR),
        Hero("Techies", Attribute.INT),
    ]

    def get_heroes(self):
        """
        Returns all the internal heroes of the class
        @return:
            - heroes: The list of heroes
        """
        return self.hero_list
