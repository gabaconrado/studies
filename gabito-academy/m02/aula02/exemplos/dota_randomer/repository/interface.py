class HeroRepository:
    """
    Defines the behavior of any hero data source
    """

    def get_heroes(self):
        """
        Returns all saved heroes
        @return:
            - heroes: The list of heroes in the data source
        """
        raise NotImplementedError("Cannot call method from the interface")
