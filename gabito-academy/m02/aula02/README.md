# M02 - Aula 02

## Tópicos

- Estrutura de um pacote
- Conversão/Avaliação implícita
- Funções
  - Argumentos
    - Obrigatórios
    - Opcionais
    - Como chamar
    - Args, Kw-args
- Classes
  - Atributos
  - Métodos
    - Métodos de instância
    - Métodos de classe
    - Métodos mágicos
    - Construtor
  - Herança/Interface

- Estruturas de dados básicas
  - Lista
  - Dicionário (HashMap)
