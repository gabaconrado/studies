# Revisão Python - 19/10/2022

## Glot.io

Ambiente para rodar coisas simples do código: [Glot](https://glot.io/)

## Básicos Python

### Interpretador

Python é uma linguagem de script. Um programa de python é uma série de comandos que é lida sequencialmente
por um interpretador.

Em um ambiente comum, criamos um arquivo `main.py` e rodamos esse arquivo com `python main.py`.

### Comandos básicos

```python
# Linhas que começam com "#" representam comentários, comentários são ignorados pelo interpretador

# Para escrever na tela
print("texto")

# Para ler a entrada do usuário
entrada = input("Texto que aparece antes da entrada")

# Para fazer um condicional
número = 10
if (número > 5):
    print("Número maior do que 5")
else:
    print("Número menor ou igual a 5")

# Para fazer um laço
números = [1, 2, 3, 4, 5]
for número in números:
    print(número)
```

### Variáveis e valores

Valor é um valor concreto que tem um tipo; Por exemplo o número 2 ou o texto "nando".

Variável é um valor salvo na memória do computador que o programador referencia por um nome arbitrário.

```python
# Exemplo: uma variável chamada "entrada" que contém o valor numérico 10
entrada = 10

# Exemplo: uma variável chamada "texto" que contém o valor de texto "nando"
texto = "nando"

# Variáveis são criadas/alteradas usando o =
número_1 = 1
número_2 = 1000

# Variáveis são acessadas pelo seu próprio nome
número_3 = número_1 + número_2 # resultado vai ser 1001
```

Todo valor possui um tipo, o tipo define o comportamento do valor e as operações possíveis que podem
ser aplicadas a ele.

```python
# str (cadeia de caracteres, simboliza um texto)
string = "caracteres"

# int (número inteiro)
inteiro = 10

# float (número real)
real = 10.5

# bool (verdadeiro ou falso)
boolean_verdadeiro = True
boolean_false = False

# None (representa um valor vazio)
vazio = None

# list (um conjunto de valores acessíveis pelo seu índice)
lista = [1, 2, "texto", True, None]
primeiro_elemento = lista[0] # 1

# dict (conjunto de valores acessíveis por uma chave)
dicionário = {"chave_1": "valor_1", 2: "valor_dois"}
elemento_um = dicionário["chave_1"]

# func (funções que são chamadas, representam blocos de código)
#   Para definir uma função
def função():
    print("essa função imprime na tela")
#   Para chamar uma função
função()
#   Funções podem ter parâmetros
def função_com_parâmetro(parâmetro):
    print(parâmetro)
#   Para chamar a função com parâmetro
função("hello")
```
