# Módulo 2 - Python

## Aula 01 - Introdução

- Linguagem compilada x Linguagem interpretada;
- O interpretador de Python;
- O Coletor de lixo;
  - Contagem de referências;
  - Objetos são estruturas na memória;
- Tipagem dinâmica;
- Hello world;
- Variáveis e os seus tipos;
- Estruturas de controle:
  - if;
  - while;
  - for;
- O arquivo `__init__.py`
- Fluxo de execução:
  - Rodar como executável;
  - Rodar como biblioteca;
