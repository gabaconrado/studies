import sys

# Constants
MISSING_PARAM_ERROR_MSG = "Erro: Número errado de argumentos"
MISSING_PARAM_ERROR_CODE = -1

def main():

    # Check if parameters was sent
    if len(sys.argv) != 6:
        print(MISSING_PARAM_ERROR_MSG)
        sys.exit(MISSING_PARAM_ERROR_CODE)

    # Gets the parameters
    hero = sys.argv[1]
    intelligence = int(sys.argv[2])
    agi = int(sys.argv[3])
    strength = int(sys.argv[4])
    main_attr = sys.argv[5]

    # Calculate the bonus of the main attribute
    if main_attr == "int":
        attr_bonus = intelligence
    elif main_attr == "str":
        attr_bonus = strength
    elif main_attr == "agi":
        attr_bonus = agi
    else:
        # If the attribute is invalid we will just use zero
        attr_bonus = 0

    # Calculate the bonus damage
    bonus_dmg = 0 + attr_bonus
    print(f"Bônus de dano de {hero} = {bonus_dmg}")

if __name__ == "__main__":
    main()
