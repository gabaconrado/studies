import sys
import random

# Constants
NO_HERO_SENT_ERROR_MSG = "Erro: Nenhum herói enviado"
NO_HERO_SENT_ERROR_CODE = -1

def main():

    # Check if heroes were sent
    if len(sys.argv) < 2:
        print(NO_HERO_SENT_ERROR_MSG)
        sys.exit(NO_HERO_SENT_ERROR_CODE)

    # Gets the list of heroes
    heroes = sys.argv[1:]

    # Get a random hero from the list
    random_hero = random.choice(heroes)

    print(f"Herói: {random_hero}")

if __name__ == "__main__":
    main()
