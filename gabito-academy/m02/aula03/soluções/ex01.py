import sys

# Constants
NO_ATTR_ERROR_MSG = "Erro: Nenhum atributo enviado"
NO_ATTR_ERROR_CODE = -1
INVALID_ATTR_ERROR_MSG = "Erro: Atributo inválido"
INVALID_ATTR_ERROR_CODE = -2

# Dictionary with attributes where the key is the name of the attribute
# and the value is its string representation
ATTRIBUTES = {
    "agi": "Agilidade",
    "str": "Força",
    "int": "Inteligência",
}

def main():

    # Check if attribute was sent
    if len(sys.argv) != 2:
        print(NO_ATTR_ERROR_MSG)
        sys.exit(NO_ATTR_ERROR_CODE)

    # Validate the sent attribute
    input_attribute = sys.argv[1]
    attribute = ATTRIBUTES.get(input_attribute)
    if not attribute:
        print(INVALID_ATTR_ERROR_MSG)
        sys.exit(INVALID_ATTR_ERROR_CODE)

    # Prints the attribute
    print(f"{input_attribute} = {attribute}")

if __name__ == "__main__":
    main()
