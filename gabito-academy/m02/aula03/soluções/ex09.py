# Hero classes
class Hero:
    """
    Base class for a Hero
    """

    def __init__(self, name):
        self.name = name

    def move(self):
        """
        Describes how a hero moves
        """
        raise NotImplementedError("Cannot call methods from the base class")

class HorseHero(Hero):
    """
    Class for heroes that moves by horse
    """

    def move(self):
        """
        Describes how a horse heroes move around
        """
        print(f"{self.name}: Anda de cavalo")

class FloatingHero(Hero):
    """
    Class for heroes that float
    """

    def move(self):
        """
        Describes how a floating heroes move around
        """
        print(f"{self.name}: Flutua")

class WalkingHero(Hero):
    """
    Class for heroes that walk
    """

    def move(self):
        """
        Describes how a walking heroes move around
        """
        print(f"{self.name}: Anda com os pés")

# Constants
HERO_LIST = [
    HorseHero("Kotl"),
    HorseHero("Abaddon"),
    FloatingHero("Lich"),
    WalkingHero("Dragon Knight")
]

def main():
    for hero in HERO_LIST:
        hero.move()

if __name__ == "__main__":
    main()
