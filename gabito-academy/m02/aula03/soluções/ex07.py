import sys

# Constants
NO_ATTR_SENT_ERROR_MSG = "Erro: Nenhum atributo foi enviado"
NO_ATTR_SENT_ERROR_CODE = -1

# Number of heroes
HEROES_COUNT = 123
AGI_HEROES_COUNT = 38
STR_HEROES_COUNT = 42
INT_HEROES_COUNT = 43

def main():

    # Check if heroes were sent
    if len(sys.argv) != 2:
        print(NO_ATTR_SENT_ERROR_MSG)
        sys.exit(NO_ATTR_SENT_ERROR_CODE)

    # Gets the atribute
    attribute = sys.argv[1]

    # Gets the selected attribute hero count
    if attribute == "agi":
        sample_count = AGI_HEROES_COUNT
    elif attribute == "str":
        sample_count = STR_HEROES_COUNT
    elif attribute == "int":
        sample_count = INT_HEROES_COUNT
    else:
        # If the attribute is invalid, we just return 0
        sample_count = 0

    # Calculate the chance
    chance = float(sample_count) / float(HEROES_COUNT)
    chance_percentage = chance * 100

    print(f"Chance = {sample_count}/{HEROES_COUNT} = {'%.2f' % chance_percentage}%")

if __name__ == "__main__":
    main()
