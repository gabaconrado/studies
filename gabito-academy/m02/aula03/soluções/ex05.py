import sys

# Constants
NO_ATTR_SENT_ERROR_MSG = "Erro: Nenhum atributo enviado"
NO_ATTR_SENT_ERROR_CODE = -1

def main():

    # Check if attributes were sent
    if len(sys.argv) < 2:
        print(NO_ATTR_SENT_ERROR_MSG)
        sys.exit(NO_ATTR_SENT_ERROR_CODE)

    # Gets the list of attributes
    attributes = sys.argv[1:]

    # Count the attributes
    intelligence = 0
    agi = 0
    strength = 0
    for attribute in attributes:
        if attribute == "agi":
            agi += 1
        elif attribute == "str":
            strength += 1
        elif attribute == "int":
            intelligence += 1

    print(f"Atributes:\n\tAgi: {agi}\n\tStr: {strength}\n\tInt: {intelligence}")

if __name__ == "__main__":
    main()
