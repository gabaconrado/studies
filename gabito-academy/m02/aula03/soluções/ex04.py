import sys

# Constants
NO_HERO_SENT_ERROR_MSG = "Erro: Nenhum herói enviado"
NO_HERO_SENT_ERROR_CODE = -1

def main():

    # Check if heroes were sent
    if len(sys.argv) < 2:
        print(NO_HERO_SENT_ERROR_MSG)
        sys.exit(NO_HERO_SENT_ERROR_CODE)

    # Gets the list of heroes
    heroes = sys.argv[1:]

    # Sort the list of heroes
    heroes = sorted(heroes)

    print(f"Heróis: {', '.join(heroes)}")

if __name__ == "__main__":
    main()
