import sys

# Constants
NO_HERO_SENT_ERROR_MSG = "Erro: Nenhum herói enviado"
NO_HERO_SENT_ERROR_CODE = -1

def main():

    # Check if heroes were sent
    if len(sys.argv) != 2:
        print(NO_HERO_SENT_ERROR_MSG)
        sys.exit(NO_HERO_SENT_ERROR_CODE)

    # Gets the hero
    hero = sys.argv[1]

    # Revert the hero name
    reverted_hero = hero[::-1]

    print(f"Nome invertido: {reverted_hero}")

if __name__ == "__main__":
    main()
