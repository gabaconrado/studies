import sys

# Constants
WRONG_ARGC_ERROR_MSG = "Erro: Nenhum texto (ou muito texto) foi enviado"
WRONG_ARGC_SENT_ERROR_CODE = -1


def main():

    # Check if heroes were sent
    if len(sys.argv) != 2:
        print(WRONG_ARGC_ERROR_MSG)
        sys.exit(WRONG_ARGC_SENT_ERROR_CODE)

    # Gets the atribute
    text = sys.argv[1]


    # Replace the chars
    leet_text = text.replace(
        "A", "a"
    ).replace(
        "E", "e"
    ).replace(
        "I", "i"
    ).replace(
        "T", "t"
    ).replace(
        "O","o"
    ).replace(
        "a", "4"
    ).replace(
        "e", "3"
    ).replace(
        "i", "1"
    ).replace(
        "t", "7"
    ).replace(
        "o", "0"
    )

    print(f"Leet = {leet_text}")

if __name__ == "__main__":
    main()
