import time

# Hero classes
class Hero:
    """
    Hero class
    """

    def __init__(self, name, initial_dmg, initial_hp):
        self.name = name
        self.dmg = initial_dmg
        self.hp = initial_hp

    def attack(self, other):
        """
        Attacks another hero
        """
        other.hp -= self.dmg

    def is_dead(self):
        """
        Checks if the hero is dead, returning True if it is, False otherwise
        """
        return self.hp <= 0

    def __str__(self):
        """
        Human-readable representation of a hero
        """
        return f"{self.name}"

def check_hero_dead(hero_a, hero_b):
    """
    Checks if one of the heroes is dead, returning a tuple indicating which one died:
    Example:
        - if hero a is dead, it will return (True, False)
        - if hero b is dead, it will return (False, True)
        - if neither is dead, it will return (False, False)
        - if both are dead, it will return (True, True)
    """
    return (hero_a.is_dead(), hero_b.is_dead())

def main():
    # Read user input
    hero_a_name = input("Digite o nome do primeiro herói: ")
    hero_a_dmg = int(input("Digite o dano inicial do primeiro herói: "))
    hero_a_hp = int(input("Digite o HP inicial do primeiro herói: "))
    hero_b_name = input("Digite o nome do segundo herói: ")
    hero_b_dmg = int(input("Digite o dano inicial do segundo herói: "))
    hero_b_hp = int(input("Digite o HP inicial do segundo herói: "))

    # Create Heroes
    hero_a = Hero(hero_a_name, hero_a_dmg, hero_a_hp)
    hero_b = Hero(hero_b_name, hero_b_dmg, hero_b_hp)

    # Start duel
    turn = 0
    while(True):
        # Check if the fight is over by testing all cases
        if (hero_a.is_dead() and hero_b.is_dead()):
            # First: both heroes are dead
            print("Os dois heróis morreram, ninguém venceu o duelo")
            break
        elif (hero_a.is_dead()):
            # Second: hero a has died
            print(f"{hero_a} morreu. {hero_b} venceu o duelo com {hero_b.hp} de HP restante")
            break
        elif (hero_b.is_dead()):
            # Second: hero b has died
            print(f"{hero_b} morreu. {hero_a} venceu o duelo com {hero_a.hp} de HP restante")
            break

        # Sleep 1s to give a real-time effect
        time.sleep(1.0)

        # If the fight is not over, run another turn
        turn += 1
        print(f"\n---- Turno {turn} ----")
        hero_a.attack(hero_b)
        print(f"{hero_a} deu {hero_a.dmg} de dano em {hero_b}")
        hero_b.attack(hero_a)
        print(f"{hero_b} deu {hero_b.dmg} de dano em {hero_a}")


if __name__ == "__main__":
    main()
