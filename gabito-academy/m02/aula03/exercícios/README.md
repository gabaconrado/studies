# Aula 03 - Exercícios

Os exercícios foram adaptados do site [Python Brasil][1].

1. Faça um programa que verifique se um atributo existe no dota

    ```bash
    # Operação normal
    python ex01.py agi
    $ agi = Agilidade
    python ex01.py str
    $ str = Força
    python ex01.py int
    $ int = Inteligência

    # Tratamento de erros
    python ex01.py alskdjf
    $ Atributo inválido
    python ex01.py
    $ Erro: Nenhum atributo enviado
    ```

1. Faça um programa que calcule o bônus de dano de um herói a partir dos seguintes parâmetros:
    - Um herói;
    - Seus atributos (int, agi e str, respectivamente);
    - Seu atributo principal;

    ```bash
    # Fórmula: bônus dano = 0 + <atributo principal>
    # Operação normal
    python ex02.py meepo 10 20 30 agi
    $ Bônus de dano de Meepo = 20

    # Tratamento de erros
    python ex02.py
    $ Erro: Número errado de argumentos
    python ex02.py pudge 30 20
    $ Erro: Número errado de argumentos
    ```

1. Faça um programa que receba uma lista de heróis e retorne um aleatoriamente:

    ```bash
    python ex03.py meepo pudge techies dark-willow
    $ Herói: techies

    # Tratamento de erros
    python ex03.py
    $ Erro: Nenhum herói enviado
    ```

1. Faça um programa que receba uma lista de heróis e a reescreva em ordem alfabética:

    ```bash
    python ex04.py pudge meepo techies alchemist
    $ Heróis: alchemist, meepo, pudge, techies

    # Tratamento de erros
    python ex04.py
    $ Erro: Nenhum herói enviado
    ```

1. Faça um programa que receba uma lista de atributos e informe o número agregado de vezes que cada um apareceu:

    ```bash
    python ex05.py agi agi str str int int agi str int agi
    $ Atributos:
    $   Agi: 4
    $   Str: 3
    $   Int: 3

    # Tratamento de erros
    python ex05.py
    $ Erro: Nenhum atributo enviado
    ```

1. Faça um programa que inverta o nome do herói informado:

    ```bash
    python ex06.py meepo
    $ Nome invertido: opeem

    # Tratamento de erros
    python ex06.py
    $ Erro: Nenhum herói enviado
    ```

1. Faça um programa que calcule a chance de escolher um herói de um determinado atributo:

    ```bash
    python ex07.py agi
    $ Chance = 38/123 = 30,89%

    # Tratamento de erros
    python ex07.py
    $ Erro: Nenhum atributo foi enviado
    ```

1. Faça um `Leet Speak Generator`. Um programa que troca algumas letras específicas em um texto:
    - Troque 'A' ou 'a' por '4';
    - Troque 'E' ou 'e' por '3';
    - Troque 'I' ou 'i' por '1';
    - Troque 'T' ou 't' por '7';
    - Troque 'O' ou 'o' por '0';

    ```bash
    python ex08.py "Ninguem segura os guri da FoxT"
    $ Leet = "N1ngu3m s3gur4 0s gur1 d4 F0x7"

    # Tratamento de erros
    python ex08.py
    $ Erro: Nenhum texto (ou muito texto) foi enviado

    python ex08.py Ninguem segura os guri da FoxT
    $ Erro: Nenhum texto (ou muito texto) foi enviado
    ```

1. Faça um programa que crie alguns heróis internamente e descreva como eles se movimentam:
    - Não é necessário ler a lista de heróis pela linha de comando, a lista pode ser uma constante dentro do programa;
    - Sugestão: Crie uma interface `Hero` com um método vazio `move` e crie classes específicas de heróis que implementam o meio de transporte;
    - Durante a execução, escreva na tela o nome do herói e como ele se movimenta;

    ```bash
    python ex09.py
    $ Kotl: Anda de cavalo
    $ Abaddon: Anda de cavalo
    $ Lich: Flutua
    $ Dragon Knight: Anda com os pés
    ```

1. Faça um programa que simule uma batalha entre dois heróis:
    - Leia dentro do programa o dano e HP inicial dos dois heróis;
    - Escreva na tela o que ocorreu em cada turno, e o vencedor ao final;
    - Sugestão: Para dar um efeito de tempo real, coloque um tempo de espera entre os rounds;
    - Podemos considerar que os dois heróis se atacam ao mesmo tempo;

    ```bash
    python ex10.py
    $ Digite o nome do primeiro herói: <entrada-do-usuário>
    $ Digite o dano inicial do primeiro herói: <entrada-do-usuário>
    $ Digite o HP inicial do primeiro herói: <entrada-do-usuário>
    $ Digite o nome do segundo herói: <entrada-do-usuário>
    $ Digite o dano inicial do segundo herói: <entrada-do-usuário>
    $ Digite o HP inicial do segundo herói: <entrada-do-usuário>
    $
    $ ---- Turno 1 ----:
    $ <nome-herói-1> deu <dano> de dano em <nome-herói-2>
    $ ...
    $ ...
    $ <nome-herói-perdedor> morreu.
    $ <nome-herói-vencedor> venceu o duelo com <hp-herói-vencedor> de HP restante
    $ OU
    $ Os dois heróis morreram, ninguém venceu o duelo
    ```

[1]: https://wiki.python.org.br/ListaDeExercicios
