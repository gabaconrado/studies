# Ambiente

- VsCode
  - Explorador de arquivos (raiz é o diretório aberto);
  - Terminal integrado (WSL: raiz é o diretório aberto);

## Vs Code

### Dicas

- Sempre abra a pasta do projeto que está trabalhando
- `Control + "` abre/fecha o terminal integrado;
- `Control + b` abre/fecha explorador de aquivos;

## Comandos úteis de terminal

- ls: lista os arquivos e pastas do diretório atual;
- cd: troca de diretório (`..` é o diretório pai);
- pwd: mostra o diretório atual;
- ./ (arquivo): roda arquivos do diretório atual;
- python3 (arquivo): roda arquivos python do diretório atual;

```bash
# mostrar conteúdos da pasta
ls

# ir para o diretório pai
cd ..
# entrar no diretório aula04
cd aula04

# mostrar diretório atual
pwd

# rodar um arquivo chamado manage.py que está na mesma pasta
./manage.py

# rodar um script chamado script.py que está na mesma pasta
python3 script.py

# apagar um aquivo chamado script.py
rm script.py
# apagar uma pasta chamada pasta
rm -r pasta
```

## Python

### Estrutura de um projeto (Django chamado mysite com aplicativo myapp)

```text
|
|---mysite
|   |
|   |------venv (virtual environment)
|   |
|   |------manage.py (controlador do Django)
|   |
|   |------mysite (configuração do mysite)
|   |
|   |------myapp (configuração do myapp)
```

### Passo a passo criar um projeto Django

1. Criar o diretório pro novo projeto pela interface do VsCode;
1. Usar o `Open Folder` para ir para a pasta do projeto;
1. Criar o virtual environment no terminal

  ```bash
  python3 -m venv ./venv
  ```

1. (Opcional) Setar o seu vscode para usar o Python do venv (Control + Shift + p -> Python: Select Interpreter -> colocar `venv/bin/python3`)
1. Ativar o venv:

  ```bash
  source ./venv/bin/activate
  ```

1. Instalar Django (apenas na primeira vez):

  ```bash
  pip install Django
  ```

1. Criar o site:

  ```bash
  django-admin startproject mysite .
  ```

1. Criar o aplicativo:

  ```bash
  ./manage.py startapp myapp
  ```

1. Para testar:

  ```bash
  ./manage.py runserver
  ```

### Passo a passo abrindo um projeto já iniciado

1. Abra a pasta do projeto pelo VsCode `Open Folder`;
1. Abra o terminal integrado e ative o virtual env (se fez o passo do select interpreter talvez já esteja ativado):

  ```bash
  source venv/bin/activate
  ```

1. Para testar:

  ```bash
  ./manage.py runserver
  ```
