# Leet text generator: ltg
#
# Generates leet text from an input string
#
# Examples:
#
# python3 ltg "Ninguem segura os guri"
#
# outputs -> "N1ngu3m s3gur4 0s gur1"

import sys

def main():
    # argv = ltg.py <texto>
    received_args = len(sys.argv)
    if received_args != 2:
        sys.exit(f"Wrong argument count: {received_args} when it should be 2")

    text = sys.argv[1]
    new_text = ""

    # 2 garantias:
    #   1 - Cada iteração vai mudar o valor da variável "character"
    #   2 - Quando a lista acabar o laço também acaba
    for character in text:
        # Em python, somar 2 strings significa concatená-las
        if character == "a" or character == "A":
            new_text += "4"
        else:
            new_text += character

    # Desafios:
    # 1 - terminar nessa logica
    # 2 - fazer o mesmo programa mas sem criar outra variável
    # 3 - fazer o mesmo programa sem nenhum if e sem for

    print(f"Leet text: {new_text}")

if __name__ == "__main__":
    main()
