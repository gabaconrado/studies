# Aula 01

## Conceitos

- Linguagem compilada x Linguagem interpretada;
- O interpretador de Python;
- O Coletor de lixo;
  - Contagem de referências;
  - Objetos são estruturas na memória;
- Variáveis;
- Tipagem dinâmica;
- Hello world;

## Exercícios

1. Qual o valor final das variáveis x e y no bloco de código abaixo:

    ```python
    def main():
        x = 1
        y = 2

        x = x + y
        y = 0

        y = "olá"
    ```

1. Qual o valor final das variáveis x no bloco de código abaixo:

    ```python
    def soma_um(num):
        num += 10
        return num

    def main():
        y = 2
        y = soma_um(y)
    ```

1. Qual o valor final das variáveis x e y no bloco de código abaixo:

    ```python
    def soma_um(num):
        num += 1

    def main():
        x = 1
        y = 2

        x = soma_um(x)
        y = soma_um(y)
    ```

1. Qual é o tipo das variáveis no bloco de código abaixo:

    ```python
    a = 5
    b = "text"
    c = 1.0
    d = [1, 2,]
    e = (1, 2,)
    f = {1: 1, 2: 2}
    g = False
    h = None
    ```

1. Faça um script que leia o texto do usuário até ele digitar "q". Quando o usuário
digitar "q" o programa deve ser encerrado.

1. Faça um script que escreva na tela todos os números pares a partir de 0 até um
número que o usuário digitar.
