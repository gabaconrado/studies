# Soluções Exercícios

1. Qual o valor final das variáveis x e y no bloco de código abaixo:

    ```python
    x = 3
    y = "olá"
    ```

1. Qual o valor final das variáveis x e y no bloco de código abaixo:

    ```python
    y = 2
    ```

1. Qual o valor final das variáveis x e y no bloco de código abaixo:

    ```python
    x = None
    y = None
    ```

1. Qual é o tipo das variáveis no bloco de código abaixo:

    ```python
    a = int
    b = str
    c = float
    d = list
    e = tuple
    f = dict
    g = bool
    h = NoneType
    ```

1. Faça um script que leia o texto do usuário até ele digitar "q". Quando o usuário
digitar "q" o programa deve ser encerrado.

    ```python
    def main():
        texto = "a"
        while(texto != "q"):
            texto = input("Digite o texto:")

    main()
    ```

1. Faça um script que escreva na tela todos os números pares a partir de 0 até um
número que o usuário digitar.

    ```python
    def main():
        num = int(input("Digite o número: "))
        for i in range(num):
            if (i % 2 == 0):
                print(f"Valor: {i}")

    main()
    ```
