#include <stdio.h>

// Função exemplo
// Recebe um nome como entrada e escreve "Hello <nome>, world!"
void hello_world(char* name) {
    printf("Hello %s, world!\n", name);
}

int main(int argc, char **argv) {
    // Declarando variável
    int x = 0;
    // Laço for (de 0 a 19 (20 iterações))
    for (x = 0; x < 20; x++) {
        // Escrever um inteiro na tela
        printf("%d\n", x);
    }

    double real = 0.5;
    // Escrever um número real na tela, com 2 casas decimais
    printf("%.2f\n", real);

    if (argc > 1) {
        // Acessando o primeiro argumento de entrada
        char* primeiro_argumento = argv[1];
        // Acessando o último argumento de entrada, -1 pois retiramos o primeiro argumento, que é o próprio executável
        // Por exemplo:
        // ./programa arg1      arg2      arg3
        // argv[0]    argv[1]   argv[2]   argv[3]
        // argc = 4
        char* ultimo_argumento = argv[argc - 1];
        // Escrever uma cadeia de caracteres na tela ('\n' server para pular uma linha)
        printf(
            "Primeiro argumento: %s\nÚltimo argumento: %s\n",
            primeiro_argumento, 
            ultimo_argumento
        );
    }

    // Chamada de função
    hello_world("Thiago");
    hello_world("Lima");

    return 0;
}