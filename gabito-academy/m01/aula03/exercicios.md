# Exercícios (11-02-2022)

Exercícios propostos para vocês fazerem :)

Está em ordem de dificuldade, podemos fazer juntos alguns semana que vem.

## Exercício 1

Escreva um programa para ler vários números inteiros como argumentos e escrever na tela o maior deles.
Não é necessário validar os argumentos de entrada, assuma que serão sempre números inteiros.

### Exemplos de uso

```bash
./maior 2 5 1 2 3 9 0 7 20
$ Maior valor: 20

./maior 11 88 92 -42 0 9 81 2
$ Maior valor: 92
```

### Dicas

- Não se esqueça de converter os argumentos da linha de comando em inteiros usando a função [atoi](https://www.tutorialspoint.com/c_standard_library/c_function_atoi.htm);
- Não se esqueça de importar a biblioteca que contém a função `atoi` com `#include <stdlib.h>`;
- Você pode usar um laço de repetição [for](https://www.programiz.com/c-programming/c-for-loop) utilizando `argc` e `argv` para ler todos os argumentos;
- Para acessar um elemento de vetor use a sintaxe `argv[<posição>]`, onde posição é o número inteiro (iniciado em 0), que você quer acessar;
- Existe um template básico de programa em C chamado `template.c` nesse mesmo diretório;

## Exercício 2

Escreva um programa para calcular o teorema de Pitágoras (valor da hipotenusa), a partir dos valores
dos dois catetos de um triângulo retângulo. Não é necessário validar se os argumentos de entrada são
números válidos, assuma que serão sempre números inteiros.

### Exemplos de uso

```bash
./pitagoras 3 4
$ Valor da hipotenusa: 5

./pitagoras 4 5
$ Valor da hipotenusa: 6.40
```

### Dicas

- Não se esqueça de converter os argumentos da linha de comando em inteiros usando a função [atoi](https://www.tutorialspoint.com/c_standard_library/c_function_atoi.htm);
- Não se esqueça de importar a biblioteca que contém a função `atoi` com `#include <stdlib.h>`;
- Será necessário usar o tipo `float` ou `double` dos [tipos disponíveis de C](http://linguagemc.com.br/tipos-de-dados-em-c/) para a hipotenusa;
- Para exponenciação e raíz quadrada, use as funções `sqrt` e `pow` da biblioteca [math](https://www.programiz.com/c-programming/library-function/math.h);
- Não se esqueça de importar a biblioteca que contém as funções matemáticas com `#include <math.h>`;
- Existe um template básico de programa em C chamado `template.c` nesse mesmo diretório;

## Exercício 3

Escreva um programa que recebe vários heróis de dota como argumentos e retorna um aleatório. Não é
necessário validar se os argumentos passados são heróis válidos.

### Exemplos de uso

```bash
./dota-randomer alchemist drow bounty enigma
$ enigma

./dota-randomer pudge pudge pudge meepo pudge pudge
$ meepo
```

### Dicas

- Para gerar um número aleatório, use as funcões `srand` e `rand` em conjunto, como nesse [exemplo](https://www.geeksforgeeks.org/rand-and-srand-in-ccpp/);
- É possível indicar o número máximo do gerador aleatório usando o operador matemático de resto `%`; Por exemplo `rand() % 5` gerará um número entre 0 e 5;
- Não se esqueça de importar a biblioteca que contém as funções `srand` e `rand` com `#include <stdlib.h>`;
- Não se esqueça de importar a biblioteca que contém a função `time` usada na inicialização de `srand` com `#include <time.h>`
- Existe um template básico de programa em C chamado `template.c` nesse mesmo diretório;