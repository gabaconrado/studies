# Aula 02 - Linguagem C - 06/02/2022

## Conceitos

- Modularização: É importante que o código seja fácil de ler e entender;
- Dividir em funções e módulos é importante para garantir a modularização;

### Funções

Funções são blocos de instruções de código que são salvos na memória, como uma variável,
só que ao invés de guardar um valor, guarda uma sequência de instruções (jumps, condicionais, chamadas de sistema, etc...).

Uma função é identificada por sua **assinatura**, formada por:

```c
// Tipo de retorno, nome, argumentos;
int func();
void func2(int arg1, char arg2);
```

A função especial **main** é definida como o ponto de entrada de um programa, e ela tem a seguinte
assinatura:

```c
int main(int argc, char **argv) {}
```

Onde:

- **argc** guarda a quantidade de argumentos de linha de comando que foram enviados ao programa;
- **argv** guarda os argumentos em si;

Exemplo:

```bash
# Rodando um programa já compilado passando argumentos em sequência
./programa argumento1 argumento2 argumento3

Argc = 4
Argv0 = ./programa
Argv1 = argumento1
Argv2 = argumento2
Argv3 = argumento3
```

### Arrays/Vetores

Um array é um conjunto de valores do mesmo tipo, também chamado de vetor. O valor [0, 1, 2, 3, 4, 5]
é um vetor de números inteiros.

Em C, um vetor é um ponteiro para o seu primeiro elemento, e os outros elementos estão dispostos
em sequência na memória. É responsabilidade do programador saber o tamanho do vetor e garantir que
nenhuma leitura/escrita será feita fora da área correta na memória.

Exemplos:

```c
// Vetor de números inteiros com 5 posições
int vetor_inteiro[5];
for (int i = 0; i < 5; i++) {
    vetor_inteiro[i] = i;
}
// Resultado final vai ser = [0, 1, 2, 3, 4]
```

### Strings

Não existe um tipo de texto em C, textos são representados como um vetor de caracteres.

O argumento da função main **argv** é um vetor de textos, ou seja, um vetor de vetores de caracteres.

```c
// Uma palavra é um vetor de caracteres finalizada com um caractere especial \0
char palavra[4] = {'g', 'a', 'b', 'a'};
// Também pode ser inicializado com aspas duplas 
char palavra2[4] = "gaba";
// Um vetor de palavras é um vetor de vetor de caracteres
char palavras[2][4] = {palavra, palavra2};
```

### Bibliotecas

Bibliotecas são arquivos com extensão `.h`. Eles listam tipos e assinaturas de funções que podem
ser implementados em arquivos `.c` separadamente. Esses arquivos de cabeçalho definem uma interface
de um componente que pode ser usada em outros componentes.

### Prática - Calculadora

No discord há um exemplo de uma calculadora feita para apresentar os conceitos básicos.