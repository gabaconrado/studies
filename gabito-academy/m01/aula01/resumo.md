# Aula 01 - Introdução - 29/01/2022

## Conceitos

### Programação

> Um programa é uma conjunto de bits em um formato específico que roda em um ambiente computacional.

O controle de fluxo de um software pode ser resumido em três funções principais:

1. **Branching**: Capacidade de condicionalmente executar ou pular um bloco de instruções;
1. **Loop**: Capacidade de repetir (in)definidamente um bloco de instruções;
1. **Jump**: Capacidade de se movimentar arbitrariamente entre blocos de instruções;

Com o controle de fluxo, temos um conjunto sequencial das seguintes instruções básicas:

1. **Ler da memória**: Capacidade de ler dados de um endereço de memória;
1. **Escrever na memória**: Capacidade de escrever dados em um endereço de memória;
1. **Chamada de sistema**: Capacidade de fazer requisições por recursos do ambiente computacional;

Na execução de um software, o ambiente computacional carrega todas as suas instruções na memória
e começa a executá-las sequencialmente até o seu fim, retornando um código de execução que sinaliza
sucesso ou falha.

### Compiladores

Softwares são feitos a partir de outros softwares, para que esse processo seja relativamente simples,
é necessário que existam programas capazes de traduzir linguagens naturais (inglês) para a linguagem
de máquina (conjunto de bits interpretáveis pelo computador).

Esse programa tradutor é chamado de **compilador**.

Os compiladores recebem um arquivo de texto escrito em linguagem natural, que é chamado de _código fonte_,
e produzem um arquivo binário que pode ser executado no ambiente computacional. O código fonte precisa
seguir um formato e convenções especificadas pelo compilador para que o processo seja feito com sucesso.
Cada compilador e linguagem de programação possuem características e especificações diferentes.

### Memória

A memória é onde todos os dados são carregados e escritos durante a execução de um software, é interessante
conhecer o conceito de memória interna (_stack_) e memória externa (_heap_).

1. **Stack**: Mais rápida, mais restrita, menor capacidade, gerenciada pelo ambiente computacional;
1. **Heap**: Mais lenta, menos restrita, maior capacidade, gerenciada pelo software via chamadas de sistema;

## Linguagem de programação C

### Hello world

```c
// Necessário para trazer a função "printf" ao escopo
#include <stdio.h>

// Definição do ponto de entrada do programa
int main()
{
    // Chamada da função de escrita na tela
    printf("Hello, world!");
}
```
### Palavras reservadas

O controle de fluxo, além das funcionalidades básicas da linguagem são feitos por palavras reservadas:

```c
// Controle de fluxo
// Loops
while(<condição>) { } // Condição -> Loop
do { } while(<condição>) // Loop -> Condição
for (contador = <valor-inicial>; <condição>; <incremento>) { } // Loop com contador

// Branching
if(<condição>) { 
    // Código se verdadeira
} else (<condição>) {
    // Código se falsa
}

// Jump
goto <etiqueta>;
<etiqueta>:

// Tipos
int, char, float, double, long, short, int[], *char, unsigned long...
```

### Variáveis

Uma variável é um valor salvo em um endereço de memória e gerenciado pelo ambiente computacional,
toda variável possui um tipo e um tamanho.

**Ponteiros** são um tipo especial de variável que guardam um endereço de memória em seu valor, o tipo
de um ponteiro serve para indicar qual o tamanho esperado do valor presente no endereço para qual o
ponteiro aponta.

```c
// Para declarar uma nova variável
int a;   // Inteiro
char b;  // Caractere
int *c;  // Ponteiro para inteiro

// Para manipular o valor de uma variável
int d = 10;
int e = 10 + d;
int *f = &e; // O operador "&" lê o endereço de memória de uma variável comum
```

## Referências e links úteis

- [Tipos de dados em C](https://en.wikipedia.org/wiki/C_data_types)
- [GCC](https://gcc.gnu.org/)
- [Visual Studio Code](https://code.visualstudio.com/)
