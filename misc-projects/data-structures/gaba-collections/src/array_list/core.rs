use super::constants;

/// ArrayList data structure implementation.
///
/// ## Features
/// - O(1) insertion;
/// - O(1) get;
/// - O(1) deletion;
/// - O(n) query;
/// - O(n) Immutable iteration through `iter()`;
/// - Idiomatic indexing access;
///
/// ## Examples
///
/// ```rust
/// use gaba_collections::ArrayList;
///
/// let mut list = ArrayList::<u8>::new();
///
/// // is empty
/// assert!(list.is_empty());
///
/// // push
/// list.push(1);
/// list.push(2);
///
/// // len
/// assert_eq!(list.len(), 2);
///
/// // get
/// assert_eq!(list.get(0), Some(&1));
/// assert_eq!(list.get(1), Some(&2));
/// assert_eq!(list.get(2), None);
///
/// // delete
/// list.delete(1);
/// assert_eq!(list.get(1), None);
///
/// // search
/// assert_eq!(list.search(2), None);
/// assert_eq!(list.search(1), Some(0));
///
/// // indexing
/// assert_eq!(list[0], 1);
/// // assert_eq!(list[100], 99); // out of bounds. this will panic!
///
/// // iteration
/// let mut iter = list.iter();
/// assert_eq!(iter.next(), Some(&1));
/// assert_eq!(iter.next(), None);
/// ```
#[derive(Debug, Clone)]
pub struct ArrayList<T> {
    array: Vec<Option<T>>,
    elements: usize,
}

// ArrayList Public API
impl<T> ArrayList<T>
where
    T: Clone + PartialEq,
{
    /// Creates a new empty array list
    pub fn new() -> Self {
        let array = vec![None; constants::ARRAY_LIST_INITIAL_SIZE];
        let elements = 0;
        Self { array, elements }
    }

    /// Pushes an element into the list
    ///
    /// This function checks if it is necessary to double the length of the underlying
    /// memory array.
    /// Time complexity: O(1) amortized
    pub fn push(&mut self, element: T) {
        let should_double = self.elements == self.array.len();
        if should_double {
            self.double_array();
        }

        self.array[self.elements] = Some(element);
        self.elements += 1;
    }

    /// Gets a reference to the element at `index` position. Returns `None` if out of bounds
    ///
    /// Time complexity: O(1)
    pub fn get(&self, index: usize) -> Option<&T> {
        let out_of_bounds = index >= self.elements;
        if out_of_bounds {
            return None;
        }
        self.array[index].as_ref()
    }

    /// Deletes the element at `index` position. No operation if out of bounds
    ///
    /// This functions checks if it is necessary to halve the length of the underlying memory
    /// array.
    /// Time complexity: O(1) amortized
    pub fn delete(&mut self, index: usize) {
        let out_of_bounds = index >= self.elements;
        if out_of_bounds {
            return;
        }

        self.elements -= 1;
        self.shift_values(index);

        let should_halve = self.elements > 0 && self.elements < self.array.len() / 2;
        if should_halve {
            self.halve_array();
        }
    }

    /// Queries the list for `element`, returning its index if found and `None` otherwise.
    ///
    /// Time complexity: O(n)
    pub fn search(&self, element: T) -> Option<usize> {
        for i in 0..self.elements {
            if let Some(e) = &self.array[i] {
                if *e == element {
                    return Some(i);
                }
            }
        }
        None
    }

    /// Returns an immutable iterator from the instance
    pub fn iter(&self) -> ArrayListIter<T> {
        ArrayListIter {
            list: self,
            current_index: 0,
        }
    }

    /// Returns the number of elements in the list
    pub fn len(&self) -> usize {
        self.elements
    }

    /// Returns if the array is empty
    pub fn is_empty(&self) -> bool {
        self.len() == 0
    }
}

// ArrayList Private methods
impl<T> ArrayList<T>
where
    T: Clone,
{
    /// Doubles the lenght of the underlying memory array, copying all the current elements to it.
    fn double_array(&mut self) {
        let current_size = self.array.len();
        let new_size = current_size * 2;
        let mut new_array = vec![None; new_size];

        new_array[..current_size].clone_from_slice(&self.array);
        self.array = new_array;
    }

    /// Halves the lenght of the underlying memory array, copying all the current elements to it.
    fn halve_array(&mut self) {
        let current_size = self.array.len();
        let new_size = current_size / 2;
        let mut new_array = vec![None; new_size];

        new_array.clone_from_slice(&self.array[..new_size]);
        self.array = new_array;
    }

    /// Removes the element at `index` from the list using shifts.
    ///
    /// The removal is done by iterating through the list from `index`, copying the next value
    /// to the current position. At the end the last element is set to `None`, effectivelly
    /// decreasing the list length by one.
    fn shift_values(&mut self, index: usize) {
        let start = index;
        let end = self.elements;
        for i in start..end {
            self.array[i] = self.array[i + 1].clone();
        }
        self.array[end] = None;
    }
}

// Trait implementations
impl<T> std::ops::Index<usize> for ArrayList<T> {
    type Output = T;

    fn index(&self, index: usize) -> &Self::Output {
        self.array[index].as_ref().unwrap()
    }
}

impl<T> std::ops::IndexMut<usize> for ArrayList<T> {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        self.array[index].as_mut().unwrap()
    }
}

impl<T> std::default::Default for ArrayList<T>
where
    T: Clone + PartialEq,
{
    fn default() -> Self {
        Self::new()
    }
}

pub struct ArrayListIter<'a, T> {
    list: &'a ArrayList<T>,
    current_index: usize,
}

impl<'a, T> Iterator for ArrayListIter<'a, T> {
    type Item = &'a T;

    fn next(&mut self) -> Option<Self::Item> {
        if self.current_index == self.list.elements {
            return None;
        }

        self.current_index += 1;
        self.list.array[self.current_index - 1].as_ref()
    }
}

#[cfg(test)]
mod tests {

    use super::*;
    use rstest::*;

    type Tut = u8;
    type Sut = ArrayList<Tut>;

    #[fixture]
    fn array_list() -> Sut {
        Sut::new()
    }

    mod new {

        use super::*;

        #[test]
        fn new() {
            let al = Sut::new();

            assert_eq!(al.array, [None; constants::ARRAY_LIST_INITIAL_SIZE]);
            assert_eq!(al.elements, 0);
        }
    }

    mod push {
        use super::*;

        #[rstest]
        fn push(mut array_list: Sut) {
            let value = Some(1);

            array_list.push(1);

            assert_eq!(array_list.array[0], value);
            assert_eq!(array_list.elements, 1);
        }

        #[rstest]
        fn array_doubling(mut array_list: Sut) {
            const COUNT: usize = constants::ARRAY_LIST_INITIAL_SIZE + 1;

            for _ in 0..COUNT {
                array_list.push(1);
            }

            assert_eq!(
                array_list.array.len(),
                2 * constants::ARRAY_LIST_INITIAL_SIZE
            );
            assert_eq!(array_list.elements, COUNT);
            assert_eq!(array_list.array[0..COUNT], [Some(1); COUNT]);
        }
    }

    mod get {
        use super::*;

        #[rstest]
        fn get(mut array_list: Sut) {
            let value = Some(5);
            array_list.array[0] = value;
            array_list.elements = 1;

            let element = array_list.get(0);

            assert_eq!(element, value.as_ref());
        }

        #[rstest]
        fn out_of_bounds(array_list: Sut) {
            let element = array_list.get(10);

            assert_eq!(element, None);
        }
    }

    mod index {
        use super::*;

        #[rstest]
        fn index(mut array_list: Sut) {
            let value = 5;
            array_list.array[0] = Some(value);
            array_list.elements = 1;

            let element = array_list[0];

            assert_eq!(element, value);
        }

        #[rstest]
        fn index_mutable(mut array_list: Sut) {
            let value = 5;
            let other_value = 10;
            array_list.array[0] = Some(value);
            array_list.elements = 1;

            array_list[0] = other_value;
            let element = array_list.array[0];

            assert_eq!(element, Some(other_value));
        }

        #[rstest]
        #[should_panic]
        fn out_of_bounds(array_list: Sut) {
            array_list[0];
        }
    }

    mod delete {
        use super::*;

        #[rstest]
        fn delete(mut array_list: Sut) {
            let value = 5;
            array_list.array[0] = Some(value);
            array_list.elements = 1;

            array_list.delete(0);

            assert_eq!(array_list.elements, 0);
            assert_eq!(array_list.array[0], None);
        }

        #[rstest]
        fn shift(mut array_list: Sut) {
            array_list.array = vec![Some(0), Some(1), Some(2), Some(3)];
            array_list.elements = 4;

            array_list.delete(1);

            assert_eq!(array_list.elements, 3);
            assert_eq!(array_list.array, vec![Some(0), Some(2), Some(3), None]);
        }

        #[rstest]
        fn table_halving(mut array_list: Sut) {
            const COUNT: usize = 2 * constants::ARRAY_LIST_INITIAL_SIZE;
            let value = Some(5);
            array_list.array = vec![value; COUNT];
            array_list.elements = COUNT;

            for _ in 0..COUNT {
                array_list.delete(0);
            }

            assert_eq!(array_list.elements, 0);
            assert_eq!(array_list.array, [None; constants::ARRAY_LIST_INITIAL_SIZE]);
        }

        #[rstest]
        fn out_of_bounds(mut array_list: Sut) {
            array_list.delete(99);
        }
    }

    mod search {
        use super::*;

        #[rstest]
        fn search(mut array_list: Sut) {
            let value = 5;
            array_list.array[0] = Some(value);
            array_list.elements = 1;

            let index = array_list.search(value);
            assert_eq!(index, Some(0));
        }

        #[rstest]
        fn doesnt_exist(array_list: Sut) {
            let index = array_list.search(10);
            assert_eq!(index, None);
        }
    }

    mod default {
        use super::*;

        #[test]
        fn default() {
            let al = Sut::default();

            assert_eq!(al.array, [None; constants::ARRAY_LIST_INITIAL_SIZE]);
            assert_eq!(al.elements, 0);
        }
    }

    mod iter {
        use super::*;

        #[rstest]
        fn iter(mut array_list: Sut) {
            let count = 4;
            let value = 1;
            array_list.array = vec![Some(value); count];
            array_list.elements = count;

            for e in array_list.iter() {
                assert_eq!(*e, value);
            }
        }
    }

    mod len {
        use super::*;

        #[rstest]
        fn len(mut array_list: Sut) {
            array_list.array[0] = Some(1);
            array_list.elements = 1;

            assert_eq!(array_list.len(), 1);
        }
    }
}
