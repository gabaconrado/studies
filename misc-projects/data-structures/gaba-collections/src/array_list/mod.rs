/// Useful constants definitions
mod constants;
/// Array List core implementation
mod core;

// External exposed objects
pub use self::core::ArrayList;
