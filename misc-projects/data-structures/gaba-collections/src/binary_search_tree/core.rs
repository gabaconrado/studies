use super::node::Node;

/// The Tree data structure implementation
#[derive(Debug)]
pub struct BinarySearchTree<T> {
    root: Node<T>,
}

// Public API implementation
impl<T> BinarySearchTree<T> {
    /// Creates a new tree with the element T in its root
    pub fn new(element: T) -> Self {
        Self {
            root: Node::new(element),
        }
    }
}

// Private implementation
impl<T> BinarySearchTree<T> {}

#[cfg(test)]
mod tests {
    use super::*;
    use rstest::*;

    type Tut = u8;
    type Sut = BinarySearchTree<Tut>;

    #[fixture]
    fn tree() -> Sut {
        let element = 5;
        Sut::new(element)
    }

    mod new {
        use super::*;

        #[rstest]
        fn new() {
            let element = 5;
            let tree = Sut::new(element);

            assert_eq!(*tree.root.element(), element);
        }
    }

    mod insert {}
}
