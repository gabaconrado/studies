/// The type of each child node in the tree
type NodeType<T> = Option<Vec<Node<T>>>;

/// The Node object for the tree
#[derive(Debug)]
pub struct Node<T> {
    element: T,
    childs: NodeType<T>,
}

impl<T> Node<T> {
    /// Creates a new node from an element
    pub fn new(element: T) -> Self {
        Self {
            element,
            childs: None,
        }
    }

    /// Adds a child node to itself
    pub fn add_child(&mut self, node: Self) {
        match self.childs.as_mut() {
            None => self.childs = Some(vec![node]),
            Some(childs) => childs.push(node),
        }
    }

    /// Getter for the element
    pub fn element(&self) -> &T {
        &self.element
    }

    /// Getter for the childs
    pub fn childs(&self) -> Option<&Vec<Node<T>>> {
        self.childs.as_ref()
    }

    /// Mutable getter for the childs
    pub fn childs_mut(&mut self) -> Option<&mut Vec<Node<T>>> {
        self.childs.as_mut()
    }

    /// Returns true if the node is full given the order, false otherwise
    pub fn is_full(&self, order: u8) -> bool {
        match self.childs.as_ref() {
            None => false,
            Some(childs) => childs.len() == order as usize,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use rstest::*;

    type Tut = u8;
    type Sut = Node<Tut>;

    #[fixture]
    fn node() -> Sut {
        let element = 5;
        Sut::new(element)
    }

    mod new {
        use super::*;

        #[rstest]
        fn new() {
            let element = 5;
            let node = Sut::new(element);

            assert_eq!(node.element, element);
            assert!(node.childs.is_none());
        }
    }

    mod add_child {
        use super::*;

        #[rstest]
        fn empty(mut node: Sut) {
            let other_element = 10;
            let other_node = Sut::new(other_element);

            node.add_child(other_node);
            let childs = node.childs;
            assert!(childs.is_some());
            assert_eq!(childs.unwrap()[0].element, other_element);
        }

        #[rstest]
        fn not_empty(mut node: Sut) {
            node.childs = Some(vec![Sut::new(10)]);
            let other_element = 20;
            let other_node = Sut::new(other_element);

            node.add_child(other_node);
            let childs = node.childs.as_ref();
            assert!(childs.is_some());
            assert_eq!(childs.unwrap().len(), 2);
            assert_eq!(childs.unwrap()[1].element, other_element);
        }
    }

    mod getters {
        use super::*;

        #[rstest]
        fn element(node: Sut) {
            assert_eq!(*node.element(), 5);
        }

        #[rstest]
        fn childs_empty(node: Sut) {
            assert!(node.childs().is_none());
        }

        #[rstest]
        fn childs(mut node: Sut) {
            node.childs = Some(vec![Sut::new(10)]);

            assert!(node.childs().is_some());
            assert_eq!(node.childs().unwrap()[0].element, 10);
        }

        #[rstest]
        fn childs_mut_empty(mut node: Sut) {
            assert!(node.childs_mut().is_none());
        }

        #[rstest]
        fn childs_mut(mut node: Sut) {
            node.childs = Some(vec![Sut::new(10)]);
            let element = 20;

            assert!(node.childs_mut().is_some());
            let childs = node.childs_mut().unwrap();
            childs[0].element = element;

            assert_eq!(node.childs().unwrap()[0].element, element);
        }
    }

    mod is_full {
        use super::*;

        #[rstest]
        fn full(mut node: Sut) {
            let order = 1;
            node.childs = Some(vec![Sut::new(10)]);

            assert!(node.is_full(order));
        }

        #[rstest]
        fn not_full(mut node: Sut) {
            let order = 2;
            node.childs = Some(vec![Sut::new(10)]);

            assert!(!node.is_full(order));
        }

        #[rstest]
        fn empty(mut node: Sut) {
            let order = 1;
            node.childs = None;

            assert!(!node.is_full(order));
        }
    }
}
