#![warn(missing_docs)]

//! A crate with simple implementations for common data structures.

/// Array List implementation
mod array_list;
/// Binary Search Tree implementation
mod binary_search_tree;
/// Hash table implementation
mod hash_table;
/// Stack implementation
mod stack;

// Exposed objects
pub use array_list::ArrayList;
pub use binary_search_tree::BinarySearchTree;
pub use hash_table::HashTable;
pub use stack::Stack;
