type Pointer<T> = Option<Box<Node<T>>>;

/// Object to represent an unit in the stack
#[derive(Debug)]
pub struct Node<T> {
    element: T,
    next: Pointer<T>,
}

impl<T> Node<T> {
    /// Creates a new node from the element, the next is initialized to [`None`]
    pub fn new(element: T) -> Self {
        Node {
            element,
            next: None,
        }
    }

    /// Sets the next component to the given node
    pub fn set_next(&mut self, node: Node<T>) {
        self.next = Some(Box::new(node));
    }

    /// Gets the element held by the node
    pub fn element(&self) -> &T {
        &self.element
    }

    /// Gets the next element, removing it from the node
    pub fn next(&mut self) -> Option<Self> {
        self.next.take().map(|node| *node)
    }

    /// Destroys the node, returning its held element
    pub fn destroy(self) -> T {
        self.element
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use rstest::*;

    type Tut = u8;
    type Sut = Node<Tut>;

    #[fixture]
    fn node() -> Sut {
        let element = 5;
        Sut::new(element)
    }

    mod new {
        use super::*;

        #[rstest]
        fn new() {
            let element = 5;

            let node = Sut::new(element);

            assert_eq!(node.element, element);
            assert!(node.next.is_none());
        }
    }

    mod set_next {
        use super::*;

        #[rstest]
        fn set_next(mut node: Sut) {
            let element = 10;
            let new_node = Sut::new(element);

            node.set_next(new_node);

            assert!(node.next.is_some());
            assert_eq!(node.next.unwrap().element, element);
        }
    }

    mod getters {
        use super::*;

        #[rstest]
        fn element(node: Sut) {
            assert_eq!(node.element(), &5);
        }

        #[rstest]
        fn next(mut node: Sut) {
            let element = 10;
            let new_node = Sut::new(element);

            node.set_next(new_node);

            let next = node.next();

            assert!(node.next.is_none());
            assert!(next.is_some());
            assert_eq!(next.unwrap().element, 10);
        }
    }

    mod destroy {
        use super::*;

        #[rstest]
        fn destroy(node: Sut) {
            let v = node.destroy();
            assert_eq!(v, 5);
        }
    }
}
