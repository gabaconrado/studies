use super::Node;

type Top<T> = Option<Node<T>>;

/// Stack data structure implementation
///
/// ## Features
/// - O(1) insertion;
/// - O(1) LIFO get/deletion;
/// - O(1) Peeking at the top;
/// - O(n) iteration;
///
/// ## Examples
///
/// ```rust
/// use gaba_collections::Stack;
///
/// let mut stack = Stack::<u8>::new();
///
/// // is empty
/// assert!(stack.is_empty());
///
/// // push
/// stack.push(1);
///
/// // peek
/// assert_eq!(stack.peek(), Some(&1));
///
/// // pop
/// let value = stack.pop();
/// assert_eq!(value, Some(1));
///
/// // len
/// assert_eq!(stack.len(), 0);
///
/// // iteration (consumes the stack)
/// stack.push(1);
/// stack.push(2);
/// let mut iterator = stack.iter();
/// assert_eq!(iterator.next(), Some(2));
/// assert_eq!(iterator.next(), Some(1));
/// assert_eq!(iterator.next(), None);
///
/// ```
#[derive(Debug, Default)]
pub struct Stack<T> {
    root: Top<T>,
    elements: usize,
}

// Public API implementation
impl<T> Stack<T> {
    /// Creates a new empty Stack
    pub fn new() -> Self {
        Self {
            root: None,
            elements: 0,
        }
    }

    /// Gets a reference to the value on the top of the stack
    ///
    /// Time complexity: O(1)
    pub fn peek(&self) -> Option<&T> {
        self.root.as_ref().map(|node| node.element())
    }

    /// Pushes an element into the top of the stack
    ///
    /// Time complexity: O(1)
    pub fn push(&mut self, element: T) {
        let mut node = Node::new(element);
        match self.root.take() {
            None => self.root = Some(node),
            Some(root) => {
                node.set_next(root);
                self.root = Some(node);
            }
        }
        self.elements += 1;
    }

    /// Removes and returns the top of the stack
    ///
    /// Time complexity: O(1)
    pub fn pop(&mut self) -> Option<T> {
        self.root.take().map(|mut node| {
            self.root = node.next();
            self.elements -= 1;
            node.destroy()
        })
    }

    /// Returns the number of the elements in the stack
    pub fn len(&self) -> usize {
        self.elements
    }

    /// Returns true if the stack is empty, false otherwise
    pub fn is_empty(&self) -> bool {
        self.len() == 0
    }

    /// Consumes the stack and transforms it into an iterator
    pub fn iter(self) -> StackIterator<T> {
        StackIterator { stack: self }
    }
}

/// The iterator for a Stack
pub struct StackIterator<T> {
    stack: Stack<T>,
}

impl<T> Iterator for StackIterator<T> {
    type Item = T;

    fn next(&mut self) -> Option<Self::Item> {
        self.stack.pop()
    }
}

#[cfg(test)]
mod tests {

    use super::*;
    use rstest::*;

    type Tut = u8;
    type Sut = Stack<Tut>;

    #[fixture]
    fn stack() -> Sut {
        Sut::new()
    }

    mod new {
        use super::*;

        #[rstest]
        fn new() {
            let stack = Sut::new();
            assert!(stack.root.is_none());
            assert_eq!(stack.elements, 0);
        }
    }

    mod peek {
        use super::*;

        #[rstest]
        fn peek(mut stack: Sut) {
            let value = 1;
            stack.root = Some(Node::new(value));
            stack.elements = 1;

            assert_eq!(stack.peek(), Some(&value));
        }
    }

    mod push {
        use super::*;

        #[rstest]
        fn push_root(mut stack: Sut) {
            let value = 1;

            stack.push(value);

            assert!(stack.root.is_some());
            assert_eq!(stack.root.unwrap().element(), &value);
            assert_eq!(stack.elements, 1);
        }
    }

    mod pop {
        use super::*;

        #[rstest]
        fn pop(mut stack: Sut) {
            let value = 1;
            stack.root = Some(Node::new(value));
            stack.elements = 1;

            let v = stack.pop();

            assert!(v.is_some());
            assert_eq!(v.unwrap(), value);
            assert_eq!(stack.elements, 0);
        }

        #[rstest]
        fn empty(mut stack: Sut) {
            let v = stack.pop();

            assert!(v.is_none());
        }
    }

    mod len {
        use super::*;

        #[rstest]
        fn len(mut stack: Sut) {
            let value = 1;
            stack.root = Some(Node::new(value));
            stack.elements = 1;

            assert_eq!(stack.len(), 1);
        }

        #[rstest]
        fn empty(stack: Sut) {
            assert_eq!(stack.len(), 0);
        }

        #[rstest]
        fn is_empty(stack: Sut) {
            assert!(stack.is_empty());
        }

        #[rstest]
        fn is_not_empty(mut stack: Sut) {
            let value = 1;
            stack.root = Some(Node::new(value));
            stack.elements = 1;

            assert!(!stack.is_empty());
        }
    }

    mod iter {
        use super::*;

        #[rstest]
        fn iter(mut stack: Sut) {
            let count = 5;

            for value in (0..count).rev() {
                stack.push(value);
            }

            for (v, value) in stack.iter().enumerate() {
                assert_eq!(v as u8, value);
            }
        }
    }
}
