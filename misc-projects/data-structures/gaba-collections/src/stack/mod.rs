/// Stack core implementation
mod core;
/// Implementations of the node unit for the stack
mod node;

use node::Node;

pub use self::core::Stack;
