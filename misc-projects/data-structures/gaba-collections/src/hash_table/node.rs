#[derive(Debug, Eq, Clone)]
pub struct Node<K, V>
where
    K: PartialEq,
{
    key: K,
    value: V,
}

// Public API implementation
impl<K, V> Node<K, V>
where
    K: PartialEq,
{
    pub fn new(key: K, value: V) -> Self {
        Node { key, value }
    }

    pub fn key(&self) -> &K {
        &self.key
    }

    pub fn value(&self) -> &V {
        &self.value
    }

    pub fn value_mut(&mut self) -> &mut V {
        &mut self.value
    }
}

// Traits implementation
impl<K, V> PartialEq for Node<K, V>
where
    K: PartialEq,
{
    fn eq(&self, other: &Self) -> bool {
        self.key == other.key
    }
}

#[cfg(test)]
mod tests {

    use super::*;

    type Kut = u8;
    type Vut = u8;
    type Sut = Node<Kut, Vut>;

    mod new {
        use super::*;

        #[test]
        fn new() {
            let key = 0;
            let value = 1;

            let n = Sut::new(key, value);

            assert_eq!(n.key, key);
            assert_eq!(n.value, value);
        }
    }

    mod eq {
        use super::*;

        #[test]
        fn equals() {
            let node = Sut::new(1, 1);
            let other_node = Sut::new(1, 3);

            assert_eq!(node, other_node);
        }

        #[test]
        fn not_equals() {
            let node = Sut::new(1, 1);
            let other_node = Sut::new(2, 1);

            assert_ne!(node, other_node);
        }
    }

    mod getters {
        use super::*;

        #[test]
        fn key() {
            let key = 1;
            let value = 2;
            let node = Sut::new(key, value);

            let k = node.key();

            assert_eq!(*k, key);
        }

        #[test]
        fn value() {
            let key = 1;
            let value = 2;
            let node = Sut::new(key, value);

            let v = node.value();

            assert_eq!(*v, value);
        }
    }

    mod setters {
        use super::*;

        #[test]
        fn value_mut() {
            let key = 1;
            let value = 2;
            let new_value = 10;
            let mut node = Sut::new(key, value);

            let v = node.value_mut();
            *v = new_value;

            assert_eq!(*node.value(), new_value);
        }
    }
}
