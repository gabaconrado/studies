use std::collections::hash_map::DefaultHasher;
use std::convert::TryInto;
use std::hash::{Hash, Hasher};

use crate::ArrayList;

use super::constants;
use super::node::Node;

/// HashTable data structure implementation
///
/// ## Features
/// - O(1) insertion;
/// - O(1) get;
/// - O(1) deletion;
/// - O(n) Immutable iteration through `iter()`;
/// - Idiomatic indexing access;
///
/// ## Examples
///
/// ```rust
/// use gaba_collections::HashTable;
///
/// let mut table = HashTable::<u8, u8>::new();
///
/// // is empty
/// assert!(table.is_empty());
///
/// // insert
/// table.insert(1, 1);
/// table.insert(2, 2);
///
/// // len
/// assert_eq!(table.len(), 2);
///
/// // get
/// assert_eq!(table.get(&1), Some(&1));
/// assert_eq!(table.get(&2), Some(&2));
/// assert_eq!(table.get(&3), None);
///
/// // delete
/// table.delete(&2);
/// assert_eq!(table.get(&2), None);
///
/// // indexing
/// assert_eq!(table[1], 1);
/// // assert_eq!(table[100], 99); // out of bounds. this will panic!
///
/// // iteration
/// let mut iter = table.iter();
/// assert_eq!(iter.next(), Some((1, 1)));
/// assert_eq!(iter.next(), None);
/// ```
#[derive(Debug, Default)]
pub struct HashTable<K, V>
where
    K: PartialEq + Hash + Clone,
    V: Clone,
{
    array: Vec<ArrayList<Node<K, V>>>,
    elements: usize,
}

// Public API implementation
impl<K, V> HashTable<K, V>
where
    K: PartialEq + Hash + Clone,
    V: Clone,
{
    /// Creates a new empty Hash table
    pub fn new() -> Self {
        let array = vec![ArrayList::new(); constants::HASH_TABLE_ARRAY_INITIAL_SIZE];
        let elements = 0;
        Self { array, elements }
    }

    /// Insert an element into the hash table
    ///
    /// This function checks if it is necessary to double the length of the underlying
    /// memory array.
    /// Time complexity: O(1) amortized
    pub fn insert(&mut self, key: K, value: V) {
        if self.should_double() {
            self.double_array();
        }

        let key_hash = self.calculate_hash(&key);
        let new_node = Node::new(key, value);

        if let Some(index) = self.already_in_the_map(new_node.key(), key_hash) {
            self.array[key_hash][index] = new_node;
        } else {
            self.array[key_hash].push(new_node);
            self.elements += 1;
        }
    }

    /// Gets a reference to the element with the given key. Returns `None` if the key does not
    /// exist
    ///
    /// Time complexity: O(1)
    pub fn get(&self, key: &K) -> Option<&V> {
        let key_hash = self.calculate_hash(key);

        match self.array[key_hash].iter().find(|node| node.key() == key) {
            Some(node) => Some(node.value()),
            None => None,
        }
    }

    /// Gets a mutable reference to the element with the given key. Returns `None` if the key does
    /// not exist
    ///
    /// Time complexity: O(1)
    pub fn get_mut(&mut self, key: &K) -> Option<&mut V> {
        let key_hash = self.calculate_hash(key);

        if let Some(index) = self.already_in_the_map(key, key_hash) {
            Some(self.array[key_hash][index].value_mut())
        } else {
            None
        }
    }

    /// Deletes the element with the given key. No operation if the key does not exist
    ///
    /// This functions checks if it is necessary to halve the length of the underlying memory
    /// array.
    /// Time complexity: O(1) amortized
    pub fn delete(&mut self, key: &K) {
        let key_hash = self.calculate_hash(key);

        if let Some(index) = self.already_in_the_map(key, key_hash) {
            self.array[key_hash].delete(index);
            self.elements -= 1;

            if self.should_halve() {
                self.halve_array();
            }
        }
    }

    /// Iterable (key, value)
    pub fn iter(&self) -> HashTableIterator<K, V> {
        HashTableIterator {
            nodes: self.get_nodes(),
            current_index: 0,
        }
    }

    /// Returns the number of nodes in the hash table
    pub fn len(&self) -> usize {
        self.array.iter().fold(0, |acc, list| acc + list.len())
    }

    /// Returns if the array is empty
    pub fn is_empty(&self) -> bool {
        self.len() == 0
    }
}

// Trait implementations
pub struct HashTableIterator<K, V>
where
    K: PartialEq + Hash + Clone,
    V: Clone,
{
    nodes: Vec<Node<K, V>>,
    current_index: usize,
}

impl<K, V> Iterator for HashTableIterator<K, V>
where
    K: PartialEq + Hash + Clone,
    V: Clone,
{
    type Item = (K, V);

    fn next(&mut self) -> Option<Self::Item> {
        if self.current_index == self.nodes.len() {
            return None;
        }

        self.current_index += 1;
        let node = &self.nodes[self.current_index - 1];
        Some((node.key().clone(), node.value().clone()))
    }
}

impl<K, V> std::ops::Index<K> for HashTable<K, V>
where
    K: PartialEq + Hash + Clone,
    V: Clone,
{
    type Output = V;

    fn index(&self, index: K) -> &Self::Output {
        let el = self.get(&index);
        if el.is_none() {
            panic!("Key does not exist");
        }
        el.unwrap()
    }
}

impl<K, V> std::ops::IndexMut<K> for HashTable<K, V>
where
    K: PartialEq + Hash + Clone,
    V: Clone,
{
    fn index_mut(&mut self, index: K) -> &mut Self::Output {
        let el = self.get_mut(&index);
        if el.is_none() {
            panic!("Key does not exist");
        }
        el.unwrap()
    }
}

// Private methods implementation
impl<K, V> HashTable<K, V>
where
    K: PartialEq + Hash + Clone,
    V: Clone,
{
    /// Calculate the hash to be used as an index in an array from a given key
    fn calculate_hash(&self, key: &K) -> usize {
        let mut hasher = DefaultHasher::new();
        key.hash(&mut hasher);
        (hasher.finish() % self.array.len() as u64)
            .try_into()
            .unwrap()
    }

    /// Doubles the hash table array size
    fn double_array(&mut self) {
        let nodes = self.get_nodes();

        self.array = vec![ArrayList::new(); self.array.len() * 2 + 1];
        self.elements = 0;

        for node in nodes {
            self.insert(node.key().clone(), node.value().clone());
        }
    }

    /// Gets a vector of all nodes in the underlying arrays
    fn get_nodes(&self) -> Vec<Node<K, V>> {
        let mut nodes = Vec::new();

        for element in self.array.iter() {
            if !element.is_empty() {
                for node in element.iter() {
                    let n = node.clone();
                    nodes.push(n);
                }
            }
        }
        nodes
    }

    /// Returns if the underlying array should be doubled
    fn should_double(&self) -> bool {
        let occupancy = self.elements as f32 / self.array.len() as f32;
        occupancy >= constants::ARRAY_DOUBLING_ALPHA
    }

    /// Returns if the underlying array should be halved
    fn should_halve(&self) -> bool {
        if self.array.len() == constants::HASH_TABLE_ARRAY_INITIAL_SIZE {
            return false;
        }

        let occupancy = self.elements as f32 / self.array.len() as f32;
        occupancy < constants::ARRAY_HALVING_ALPHA
    }

    /// Halves the underlying array
    fn halve_array(&mut self) {
        let nodes = self.get_nodes();

        self.array = vec![ArrayList::new(); self.array.len() / 2];
        self.elements = 0;

        for node in nodes {
            self.insert(node.key().clone(), node.value().clone());
        }
    }

    /// Checks if a key is in one of the array lists in an index, returning the index
    /// of the internal array list at that index
    fn already_in_the_map(&self, key: &K, hash_idx: usize) -> Option<usize> {
        self.array[hash_idx]
            .iter()
            .enumerate()
            .find(|(_, node)| node.key() == key)
            .map(|(index, _)| index)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use rstest::*;

    type Kut = u8;
    type Vut = u8;
    type Sut = HashTable<Kut, Vut>;

    #[fixture]
    fn hash_table() -> Sut {
        Sut::new()
    }

    mod new {
        use super::*;

        #[test]
        fn new() {
            let ht = Sut::new();
            assert_eq!(ht.array.len(), constants::HASH_TABLE_ARRAY_INITIAL_SIZE);
            assert_eq!(ht.elements, 0);
            for e in ht.array {
                assert_eq!(e.len(), 0);
            }
        }
    }

    mod insert {
        use super::*;

        #[rstest]
        fn insert(mut hash_table: Sut) {
            let (key, value) = (1, 2);
            let key_hash = 6;

            hash_table.insert(key, value);

            assert_eq!(hash_table.elements, 1);

            let array_list = &hash_table.array[key_hash];
            assert_eq!(array_list.len(), 1);
            assert_eq!(*array_list[0].key(), key);
            assert_eq!(*array_list[0].value(), value);
        }

        #[rstest]
        fn array_doubling(mut hash_table: Sut) {
            let count = constants::HASH_TABLE_ARRAY_INITIAL_SIZE;
            let value = 2;
            for i in 0..count {
                hash_table.insert(i as u8, value);
            }

            assert_eq!(hash_table.array.len(), 2 * count + 1);
            assert_eq!(hash_table.elements, count);
        }

        #[rstest]
        fn overwrite(mut hash_table: Sut) {
            let (key, value) = (1, 2);
            let key_hash = 6;

            hash_table.insert(key, value);

            assert_eq!(hash_table.elements, 1);

            let value = 3;
            hash_table.insert(key, value);

            assert_eq!(hash_table.elements, 1);
            let array_list = &hash_table.array[key_hash];
            assert_eq!(array_list.len(), 1);
            assert_eq!(*array_list[0].key(), key);
            assert_eq!(*array_list[0].value(), value);
        }
    }

    mod get {
        use super::*;

        #[rstest]
        fn get(mut hash_table: Sut) {
            let (key, value) = (1, 2);
            let key_hash = 6;
            hash_table.array[key_hash].push(Node::new(key, value));
            hash_table.elements = 1;

            let v = hash_table.get(&key);

            assert!(v.is_some());
            assert_eq!(*v.unwrap(), value);
        }

        #[rstest]
        fn get_mut(mut hash_table: Sut) {
            let (key, value) = (1, 2);
            let new_value = 10;
            let key_hash = 6;
            hash_table.array[key_hash].push(Node::new(key, value));
            hash_table.elements = 1;

            let v = hash_table.get_mut(&key);
            assert!(v.is_some());
            *v.unwrap() = new_value;

            let v = hash_table.get(&key);
            assert!(v.is_some());

            assert_eq!(*v.unwrap(), new_value);
        }

        #[rstest]
        fn doesnt_exist(hash_table: Sut) {
            let key = 1;

            let v = hash_table.get(&key);

            assert!(v.is_none());
        }
    }

    mod delete {
        use super::*;

        #[rstest]
        fn delete(mut hash_table: Sut) {
            let (key, value) = (1, 2);
            let key_hash = 6;
            hash_table.array[key_hash].push(Node::new(key, value));
            hash_table.elements = 1;

            hash_table.delete(&key);

            assert_eq!(hash_table.array[key_hash].len(), 0);
            assert_eq!(hash_table.elements, 0);
        }

        #[rstest]
        fn array_halving(mut hash_table: Sut) {
            let count = constants::HASH_TABLE_ARRAY_INITIAL_SIZE;
            hash_table.array = vec![ArrayList::new(); count];
            for i in 0..count {
                let (key, value) = (i, 2);
                hash_table.insert(key as u8, value);
            }

            for i in 0..count {
                hash_table.delete(&(i as u8));
            }

            assert_eq!(
                hash_table.array.len(),
                constants::HASH_TABLE_ARRAY_INITIAL_SIZE
            );
        }
    }

    mod iter {
        use super::*;

        #[rstest]
        fn iterator(mut hash_table: Sut) {
            let count = 5;
            for i in 0..count {
                hash_table.insert(i, i);
            }

            let mut i = 0;
            for (key, value) in hash_table.iter() {
                assert_eq!(key, value);
                i += 1;
            }
            assert_eq!(i, count);
        }

        #[rstest]
        fn empty(hash_table: Sut) {
            for (_, _) in hash_table.iter() {
                unreachable!();
            }
        }
    }

    mod len {
        use super::*;

        #[rstest]
        fn len(mut hash_table: Sut) {
            let count = 5;
            for i in 0..count {
                hash_table.insert(i as u8, 1);
            }

            assert_eq!(hash_table.len(), count)
        }
    }

    mod index {
        use super::*;

        #[rstest]
        fn index(mut hash_table: Sut) {
            let (key, value) = (1, 2);
            hash_table.insert(key, value);

            let v = hash_table[key];

            assert_eq!(v, value);
        }

        #[rstest]
        #[should_panic]
        fn inexistent_key(hash_table: Sut) {
            let key = 1;

            hash_table[key];
        }

        #[rstest]
        fn index_mut(mut hash_table: Sut) {
            let (key, value) = (1, 2);
            let new_value = 3;
            hash_table.insert(key, value);

            hash_table[key] = new_value;

            assert_eq!(hash_table[key], new_value);
        }
    }
}
