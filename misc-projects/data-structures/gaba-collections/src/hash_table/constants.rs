pub(super) const HASH_TABLE_ARRAY_INITIAL_SIZE: usize = 7;

pub(super) const ARRAY_DOUBLING_ALPHA: f32 = 0.75;

pub(super) const ARRAY_HALVING_ALPHA: f32 = 0.25;
