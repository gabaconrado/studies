# Algorithm Complexity Analysis

## Introduction

Algorithm complexity is an important pragmatic aspect of the computer science. It is not a
requirement to be an amazing programmer, but it is a very usefull knowledge to have and definitelly
a skill that will take people to the next level.

> Algorithm Complexity Analysis is a set of methods used to determine how good an algorithm is
in terms of its time and/or space consume relative to the variation of the input.

It is not meant to be an on-point mathematical result of how many micro-seconds or bytes our
solution will use, instead it is a pragmatic and high-level evaluation of how good our solution is
and how it will behave on adverse cases.

## Counting instructions

In order to evaluate our code consumption, we must define a set of operations that will count as
instructions. This will not be exact as it depends on the architecture of each system, but it is
not relevant as we are interested in a high-level analysis.

For this arcticle I will be counting as instructions the following operations:

* Reading a value from memory(RM)
* Saving a value into memory(SM)
* Evaluating a condition(EC)
* Arithmetic operations(AO)

These instructions will be the unit to evaluate our software complexity.

To begin, let's analyze an algorithm that returns if an array contains a specific element.

```c
int containsElement(int element, int *array, int size){
    int n;
    for(n = 0; n < size; n++){
        if (element == array[n])
            return 1;
    }
    return 0;
}
```

When analyzing complexity, we will always consider the worst case scenario, it is usualy intuitive
to know what scenario is the worse for your algorithm. In this case, the worst case scenario is when
the array does not contain the element, this will give us the following situation:

```text
#1 for(n = 0; n < size; n++): 1 SM, 1 EC
#2 if (element == array[n]): 1 EC
#3 for(n = 0; n < size; n++): 1 AO, 1 SM, 1 EC
```

In the for initialization the algorithm will run 2 instructions, then it will run #2 and #3
`size` times until the end of the array. Therefore, the function that will gives us the number of
instructions in this algorithm given the **x** input is **F(x) = 2 + (1 + 3)x**.

Inspecting the function, it is possible to detect the constant part(_2_), and the input-dependent
part (_1+3_). For an array with 5 elements, 22 instructions will be executed. For an array with
1000 elements, 4002 instuctions will run, and so on. It is relatively simple to see that the
constant part will not have much influence in the overall analysis given that it is always there.
So we will just desconsider it. In the end, hence, our instruction count function is **F(x) = 4x**.

## Asymptotic Complexity

The constant part was discarded in the last example, this is commom when determining the
**Asymptotic behavior** of an algorithm. It is a mathematical idea that, when the input is big
enough, the influence of the constants is irrelevant. Hence, for our past function we could simplify
even more removing the 4 and saying that the complexity function is **F(x) = x**.

This is how all the algorithm complexity analysis is done. The goal is to have the asymptotical
behavior of a solution. There is a specific terminology for that, in computer science the greek
letter **Θ(theta)** represents the asymptotical behavior of a specific algorithm. The most commom
cases are:

* Θ(n) - Linear
* Θ(n²) - Quadratic
* Θ(logN) - Logarithmic

### Examples

To illustrate each of the above cases(linear already done above), let's analyse some functions:

### Quadratic

The quadratic case is going to be a sorting function. The most simple and inefficient of them,
named [Bubble Sort](https://pt.wikipedia.org/wiki/Bubble_sort), it will take an array and pass
through each element twice, reorganizing them as it goes.

```c
int bubbleSort(int *array, int size){
    int x, y;
    for(x = 1; x < size; x++){
        for (y = 0; y < size-1; y++){
            if(array[y] > array[y+1]){
                int aux = array[y];
                array[y] = array[y+1];
                array[y+1] = aux;
            }
        }
    }
}
```

Each iteraction of the bubble sort sorts one element, and for that it pass through the whole array.
Hence, in the worst case scenario, the algorithm will run `size * size` times. This gives us a
Θ(n^2) complexity or a quadratic complexity. Generally, a layer of for loop inside another for loop
will add one power in the complexity(Eg. 3 nested for loops will give Θ(n³)).

### Logarithmic

The quadratic case is going to be again the function that checks for a specific element in an array.
This time, however, we will have a **sorted array** as the input, and a function called
**subArray(int *array, int startIndex, int endIndex)** that returns a sub-array of the initial one.

```c
int containsElement(int element, int *array, int size){
    if (size == 1)
        return (element == array[0]);
    if (array[size/2] == element)
        return 1;
    else if (array[size/2] > element)
        return containsElement(element, subArray(array, size/2 + 1, size))
    else
        return containsElement(element, subArray(array, 0, size/2 - 1))
}
```

This is a recursive function, and in each iteration the array size is cut to its half. This way,
in the end, the complexity of the algorithm will be how many times we need to divide our input
to get our answer.

```text
n = 32: 32 -> 16 -> 8 -> 4 -> 2 -> 1

For the above case, an array of size 32, 5 divisions were needed, 5 is the logarithm of 32 in the
base 2.
```

This type of algorithm, this one is called [Binary Search](https://en.wikipedia.org/wiki/Binary_search_algorithm)
have Θ(logn) complexity or logarithmic complexity.

## Notation

The **Θ** notation reflects the exact asymptotic behavior of the algorithm, however there are some
cases where the algorithm is way to complex to find its complexity trivially. There is
some other notations derived from the **Θ** to achieve these goals.

### Big O(O)

The **O** notation(Big O) relates to the worst possible behavior of a specific algorithm. An O(n)
algorithm means that the same will never be worse than a linear complexity. It can be better or it
can be equal, but never worse. This is useful when it is too hard to get the exact complexity of
a solution. You can just add instructions or make your code less efficient in order to simplify
its analysis and then classify it using the **O** notation. This notation is the most common when
analysing complexity in computer science.

### Little O(o)

The **o** notation(Little O) also relates do the worst possible behavior of a specific algorithm.
This one, however, is not _tight_ with the algorithm. This means that the **o** notation does not
reflect reliably with the real complexity. For instance, it is correct to say that a Θ(n) algorithm
is also o(n³), it is not accurate but it is technically ok.

### Big Omega(Ω) and Little Omega(ω)

The **Ω** notation(Big Omega) is the opposite of the Big O and the **ω** is the opposite of the
Little O. They are related to the best possible behavior of an algorithm, being the Big Omega tight
and the Little Omega not. Although, they should not be confused with the best case scenarios of an
algorithm. For instance, the **Bubble Sort** algorithm is Θ(n²) in its worst case scenario and Θ(n)
in its best case scenario. Each scenario has its own complexity associated to it.

## Reference

[A gentle introduction to Algorithm Complexity Analysis](https://discrete.gr/complexity/)