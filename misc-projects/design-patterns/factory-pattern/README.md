# (Abstract) Factory Pattern

## Overview

This pattern is used when one part of your system does not actually know which specific component it will use

## Definition

> Define an interface for creating a (family of) object(s), but let the subclasses decide which class to instantiate.
> **(Abstract) Factory Pattern** lets a class defer instantiation to subclasses

![](../img/factory.png)

## Example

Imagine a system where the test classes should use some dataset. In the beggining you do not know how many or which
datasets the system will use. So, you should develop an abstract test interface that will be extended for each new
available dataset.

![](../img/factory-example.png)
