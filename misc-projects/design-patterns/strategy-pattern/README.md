# Strategy Pattern

## Overview

This pattern is used to decouple a method that depends in different aspects from a class.

## Definition

Define a family of algorithms, encapsulate each one as a sepparate them and make them interchangeable.
This reduces the complexity of the system by making these algorithms independent from the rest of the code.

In other words, **Strategy** lets the algorithm vary independently from clients that use it.

![](../img/strategy.png)

## Example

A class **EmployeeBenefits** must calculate the bonus from an **Employee**, for that it will have a method named _calculateBonus()_.

This method may vary depending on the client and/or some pre-requisites changes during the project, so it would be nice to make it independent from the rest of the code.
For now we know that the calculation can be done by merit and by grade.

To solve this we create an interface called **IBonusCalculator** and implement the classes **BonusCalculatorGrade** and **BonusCalculatorMerit** and those classes
override the base method _calculateBonus()_.

![](../img/strategy-example.png)

## Notes

- It is better to generate more code for new functionalities instead of changing actual code
- Generally it is not a good idea to have default implementations in the interface
