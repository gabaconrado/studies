use std::collections::HashMap;

pub struct Solution {}

impl Solution {
    pub fn two_sum(nums: Vec<i32>, target: i32) -> Vec<i32> {
        let mut map = HashMap::<i32, usize>::new();
        for (x, num) in nums.iter().enumerate() {
            let wanted = target - num;
            if let Some(&i) = map.get(&wanted) {
                return vec![i as i32, x as i32];
            }
            map.insert(*num, x);
        }
        vec![]
    }
}
