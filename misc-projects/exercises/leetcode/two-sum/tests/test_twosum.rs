extern crate two_sum;

use two_sum::Solution;

use rstest::*;

#[rstest]
#[case(vec![2, 7, 11, 15], 9, vec!(0, 1))]
#[case(vec![3, 2, 4], 6, vec!(1, 2))]
#[case(vec![3, 3], 6, vec!(0, 1))]
fn test_solution(#[case] nums: Vec<i32>, #[case] target: i32, #[case] expected: Vec<i32>) {
    assert_eq!(Solution::two_sum(nums, target), expected);
}
