extern crate running_sum_of_1d_array;

use rstest::*;
use running_sum_of_1d_array::Solution;

#[rstest]
#[case(vec![1, 2, 3, 4], vec![1, 3, 6, 10])]
#[case(vec![1, 1, 1, 1, 1], vec![1, 2, 3, 4, 5])]
#[case(vec![3, 1, 2, 10, 1], vec![3, 4, 6, 16, 17])]
fn test_running_sum(#[case] nums: Vec<i32>, #[case] expected: Vec<i32>) {
    assert_eq!(Solution::running_sum(nums), expected);
}
