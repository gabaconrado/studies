# Running sum of 1d array

[url](https://leetcode.com/problems/running-sum-of-1d-array/)

## Description

Given an array nums. We define a running sum of an array as runningSum[i] = sum(nums[0]…nums[i]).

Return the running sum of nums.

## Examples

```
Input: nums = [1,2,3,4]
Output: [1,3,6,10]
Explanation: Running sum is obtained as follows: [1, 1+2, 1+2+3, 1+2+3+4].
```


```
Input: nums = [1,1,1,1,1]
Output: [1,2,3,4,5]
Explanation: Running sum is obtained as follows: [1, 1+1, 1+1+1, 1+1+1+1, 1+1+1+1+1].
```

```
Input: nums = [3,1,2,10,1]
Output: [3,4,6,16,17]
```

## Constraints

```
1 <= nums.length <= 1000
-10^6 <= nums[i] <= 10^6
```

## Results

> Runtime: 0 ms, faster than 100.00% of Rust online submissions for Running Sum of 1d Array.
> Memory Usage: 2.1 MB, less than 32.40% of Rust online submissions for Running Sum of 1d Array.

## Notes

Not 100% happy with using a mutable vector, want to learn a way of building the output vector
directly from the fold expression.
