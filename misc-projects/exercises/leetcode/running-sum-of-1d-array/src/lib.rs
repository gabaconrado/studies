pub struct Solution {}

impl Solution {
    pub fn running_sum(nums: Vec<i32>) -> Vec<i32> {
        let mut sol = vec![];
        let _sum = nums.iter().fold(0, |sum, x| {
            sol.push(sum + x);
            sum + x
        });
        sol
    }
}
