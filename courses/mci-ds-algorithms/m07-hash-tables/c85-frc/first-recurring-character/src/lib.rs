use std::collections::HashSet;

/// Returns the first recurring element of the array (even if I call it character, I mean integer
/// Simplest solution:
/// - Iterate through the array, comparing each element with the whole array until a match;
/// - Complexity is O(n^2), since it is necessary to loop the array n times;
/// Better solution?:
/// - Iterate through the array, adding each element into a hash set, when an element exists,
/// returning it
/// - Complexity is O(n), since it is necessary to loop the array only once, and inserting into
/// a hash set is O(1)
/// - Space complexity is O(n), since it is necessary to create a hash set with max length of n
pub fn frc(array: &[u32]) -> Option<u32> {
    let mut hs = HashSet::<u32>::new();
    for &a in array.iter() {
        if !hs.insert(a) {
            return Some(a);
        }
    }
    None
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn should_return_first_recurring_character() {
        let array = [2, 5, 1, 2, 3, 5, 1, 2, 4];
        assert_eq!(frc(&array), Some(2));
        let array = [2, 1, 1, 2, 3, 5, 1, 2, 4];
        assert_eq!(frc(&array), Some(1));
        let array = [1, 2, 3, 4, 5];
        assert_eq!(frc(&array), None);
    }
}
