# Class 85 - First recurring character

Given an array, return the first repeated character in it.

## Examples

```txt
array = [2, 5, 1, 2, 3, 5, 1, 2, 4]
frc(array) = 2

array = [2, 1, 1, 2, 3, 5, 1, 2, 4]
frc(array) = 1

array = [2, 3, 4, 5]
frc(array) = null
```
