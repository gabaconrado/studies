use std::collections::VecDeque;

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Node {
    Node(_Node),
    Empty,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct _Node {
    pub value: u32,
    pub left: Box<Node>,
    pub right: Box<Node>,
}

impl Node {
    pub fn insert(&mut self, value: u32) {
        match self {
            Node::Node(n) => match value.cmp(&n.value) {
                std::cmp::Ordering::Less => n.left.insert(value),
                std::cmp::Ordering::Greater => n.right.insert(value),
                std::cmp::Ordering::Equal => return,
            },
            Node::Empty => {
                *self = Node::Node(_Node {
                    value,
                    left: Box::new(Node::Empty),
                    right: Box::new(Node::Empty),
                });
            }
        }
    }

    pub fn contains(&self, value: u32) -> bool {
        match self {
            Node::Node(n) => match value.cmp(&n.value) {
                std::cmp::Ordering::Less => n.left.contains(value),
                std::cmp::Ordering::Greater => n.right.contains(value),
                std::cmp::Ordering::Equal => return true,
            },
            Node::Empty => false,
        }
    }

    pub fn remove(&mut self, value: u32) -> bool {
        match self {
            Node::Node(n) => match value.cmp(&n.value) {
                std::cmp::Ordering::Less => n.left.remove(value),
                std::cmp::Ordering::Greater => n.right.remove(value),
                std::cmp::Ordering::Equal => {
                    if self.replace_with_right() {
                        return true;
                    }
                    if self.replace_with_left() {
                        return true;
                    }
                    *self = Node::Empty;
                    return true;
                }
            },
            Node::Empty => {
                return false;
            }
        }
    }

    fn replace_with_left(&mut self) -> bool {
        if let Node::Node(n) = self {
            if let Node::Node(ln) = n.left.as_ref() {
                n.value = ln.value;
                n.left = Box::new(Node::Empty);
                return true;
            }
        }
        false
    }

    fn replace_with_right(&mut self) -> bool {
        if let Node::Node(n) = self {
            if let Node::Node(ln) = n.right.as_ref() {
                n.value = ln.value;
                n.right = Box::new(Node::Empty);
                return true;
            }
        }
        false
    }

    fn bfs(&self) -> bool {
        let mut q = VecDeque::new();
        q.push_back(self);

        while let Some(Node::Node(n)) = q.pop_front() {
            println!("{}", n.value);
            if let Node::Node(ln) = n.left.as_ref() {
                if ln.value > n.value {
                    return false;
                }
                q.push_back(&n.left);
            }
            if let Node::Node(rn) = n.right.as_ref() {
                if rn.value < n.value {
                    return false;
                }
                q.push_back(&n.right);
            }
        }
        true
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct BinarySearchTree {
    pub root: Node,
    size: u32,
}

impl BinarySearchTree {
    pub fn new() -> Self {
        Self {
            root: Node::Empty,
            size: 0,
        }
    }

    pub fn insert(&mut self, value: u32) {
        self.root.insert(value);
        self.size += 1;
    }

    pub fn lookup(&self, value: u32) -> bool {
        self.root.contains(value)
    }

    pub fn remove(&mut self, value: u32) -> bool {
        let removed = self.root.remove(value);
        if removed {
            self.size -= 1;
        }
        removed
    }

    pub fn len(&self) -> u32 {
        self.size
    }

    pub fn validate(&self) -> bool {
        self.root.bfs()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn new() {
        let tree = BinarySearchTree::new();
        assert_eq!(tree.root, Node::Empty);
        assert_eq!(tree.size, 0);
    }

    #[test]
    fn len() {
        let tree = BinarySearchTree::new();
        assert_eq!(tree.len(), tree.size);
    }

    #[test]
    fn insert() {
        let mut tree = BinarySearchTree::new();
        tree.insert(10);
        assert_eq!(tree.len(), 1);
        tree.insert(20);
        assert_eq!(tree.len(), 2);
        tree.insert(5);
        assert_eq!(tree.len(), 3);
        assert_eq!(
            tree.root,
            Node::Node(_Node {
                value: 10,
                left: Box::new(Node::Node(_Node {
                    value: 5,
                    left: Box::new(Node::Empty),
                    right: Box::new(Node::Empty)
                })),
                right: Box::new(Node::Node(_Node {
                    value: 20,
                    left: Box::new(Node::Empty),
                    right: Box::new(Node::Empty)
                }))
            })
        );
    }

    #[test]
    fn lookup() {
        let mut tree = BinarySearchTree::new();
        assert!(!tree.lookup(10));
        tree.root = Node::Node(_Node {
            value: 10,
            left: Box::new(Node::Empty),
            right: Box::new(Node::Empty),
        });
        tree.size = 1;
        assert!(tree.lookup(10));
    }

    #[test]
    fn remove() {
        let mut tree = BinarySearchTree::new();
        assert!(!tree.remove(10));
        tree.root = Node::Node(_Node {
            value: 10,
            left: Box::new(Node::Node(_Node {
                value: 5,
                left: Box::new(Node::Empty),
                right: Box::new(Node::Empty),
            })),
            right: Box::new(Node::Node(_Node {
                value: 20,
                left: Box::new(Node::Empty),
                right: Box::new(Node::Empty),
            })),
        });
        tree.size = 3;
        assert_eq!(tree.len(), 3);
        assert!(tree.remove(10));
        assert_eq!(tree.len(), 2);
        assert_eq!(
            tree.root,
            Node::Node(_Node {
                value: 20,
                left: Box::new(Node::Node(_Node {
                    value: 5,
                    left: Box::new(Node::Empty),
                    right: Box::new(Node::Empty)
                })),
                right: Box::new(Node::Empty)
            })
        );
        assert!(tree.remove(20));
        assert_eq!(tree.len(), 1);
        assert_eq!(
            tree.root,
            Node::Node(_Node {
                value: 5,
                left: Box::new(Node::Empty),
                right: Box::new(Node::Empty)
            })
        );
        assert!(tree.remove(5));
        assert_eq!(tree.len(), 0);
        assert_eq!(tree.root, Node::Empty);
    }

    #[test]
    fn validate() {
        let mut tree = BinarySearchTree::new();
        tree.root = Node::Node(_Node {
            value: 10,
            left: Box::new(Node::Node(_Node {
                value: 20,
                left: Box::new(Node::Empty),
                right: Box::new(Node::Empty),
            })),
            right: Box::new(Node::Node(_Node {
                value: 5,
                left: Box::new(Node::Empty),
                right: Box::new(Node::Empty),
            })),
        });
        tree.size = 3;
        assert!(!tree.validate());
        tree.root = Node::Node(_Node {
            value: 10,
            left: Box::new(Node::Node(_Node {
                value: 5,
                left: Box::new(Node::Empty),
                right: Box::new(Node::Empty),
            })),
            right: Box::new(Node::Node(_Node {
                value: 20,
                left: Box::new(Node::Empty),
                right: Box::new(Node::Empty),
            })),
        });
        tree.size = 3;
        assert!(tree.validate());
    }
}
