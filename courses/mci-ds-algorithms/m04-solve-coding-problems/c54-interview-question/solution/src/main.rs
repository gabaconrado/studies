use std::collections::HashSet;

fn main() {
    println!("Hello, world!");
    let array1 = ['a', 'b'];
    let array2 = ['a', 'c'];
    let res = arrays_contain_common_items(&array1, &array2);
    println!("Result: {}", res);
}

fn arrays_contain_common_items(array1: &[char], array2: &[char]) -> bool {
    let set = create_set_from_array(array1);
    is_element_in_array(&set, array2)
}

fn create_set_from_array(array: &[char]) -> HashSet<char> {
    let mut set = HashSet::new();
    for c in array.iter() {
        set.insert(c.to_owned());
    }
    set
}

fn is_element_in_array(set: &HashSet<char>, array: &[char]) -> bool {
    for c in array.iter() {
        if set.contains(c) {
            return true;
        }
    }
    false
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn should_return_false() {
        let array1 = ['a', 'b', 'c', 'x'];
        let array2 = ['z', 'y', 'i'];
        assert!(!arrays_contain_common_items(&array1, &array2))
    }

    #[test]
    fn should_return_true() {
        let array1 = ['a', 'b', 'c', 'x'];
        let array2 = ['z', 'y', 'x'];
        assert!(arrays_contain_common_items(&array1, &array2))
    }
}
