# Interview exercise

Given 2 arrays, create a function that let's a user know (true/false) whether these two arrays
contain any common items.

## Example

```js
const array1 = ['a', 'b', 'c', 'x'];
const array2 = ['z', 'y', 'i'];

// should return false
```

```js
const array1 = ['a', 'b', 'c', 'x'];
const array2 = ['z', 'y', 'x'];

// should return true
```

## Solution

### Worst

First solution would be to simply compare each element of __array1__ with the __array2__ elements.
If any is equal, then we return True. This solution is O(a*b).

### Better

A better solution is to use an auxiliar __hash set__ to insert all elements of __array1__ and __array2__.
This requires additional memory, but it would bring the time complexity down to O(a+b), generating
a space complexity of also O(a+b).
