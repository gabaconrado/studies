//! Reverse a string iteratively and recursively

pub fn reverse_ite(s: &str) -> String {
    let mut n = s.chars().collect::<Vec<char>>();
    let mut r = String::new();
    for _ in 0..n.len() {
        r.push(n.pop().unwrap());
    }
    r
}

/// Reversed string:
///
/// last char + f(string)
///
/// Example:
/// abcde ->
/// e + abcd ->
/// ed + abc ->
/// edc + ab ->
/// edcb + a ->
/// edcba
pub fn reverse_rec(s: &str) -> String {
    if s.len() <= 1 {
        return s.to_string();
    }
    let mut n = s.chars().collect::<Vec<char>>();
    let last = n.pop().unwrap();
    let r = n.into_iter().collect::<String>();
    return format!("{}{}", last, reverse_rec(&r));
}

#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn should_reverse_str_iterative() {
        let s = "yoyo mastery";
        let expected = "yretsam oyoy".to_string();

        let result = reverse_ite(s);
        assert_eq!(result, expected);
    }

    #[test]
    fn should_reverse_str_recursive() {
        let s = "yoyo mastery";
        let expected = "yretsam oyoy".to_string();

        let result = reverse_rec(s);
        assert_eq!(result, expected);
    }
}
