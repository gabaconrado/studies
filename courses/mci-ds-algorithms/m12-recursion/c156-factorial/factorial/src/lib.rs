pub fn factorial_rec(n: u32) -> u32 {
    if n == 1 {
        1
    } else {
        n * factorial_rec(n - 1)
    }
}

pub fn factorial_ite(n: u32) -> u32 {
    let mut acc = 1;
    for a in 1..n + 1 {
        acc *= a;
    }
    acc
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn should_return_correct_value() {
        let n = 5;
        let result = factorial_rec(n);
        assert_eq!(result, 120);
        let result = factorial_ite(n);
        assert_eq!(result, 120);
    }
}
