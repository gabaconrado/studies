//! Fibonacci is the sum of the two previous numbers:
//!
//! 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144...
//!
//! Fibonacci(n) = Fibonacci(n - 1) + Fibonacci(n - 2);
//!
//! Constraints:
//! - n > 0;
//! - Fibonacci(0) = 0;
//! - Fibonacci(1) = 1;

pub fn fibo_rec(n: u32) -> u32 {
    if n == 0 {
        0
    } else if n == 1 {
        1
    } else {
        fibo_rec(n - 1) + fibo_rec(n - 2)
    }
}

pub fn fibo_ite(n: u32) -> u32 {
    let mut n2 = 0;
    let mut n1 = 1;
    let mut aux = 0;
    for _ in 1..n {
        aux = n1 + n2;
        n2 = n1;
        n1 = aux;
    }
    aux
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn should_return_correct_value() {
        let n = 12;

        let result = fibo_rec(n);
        assert_eq!(result, 144);

        let result = fibo_ite(n);
        assert_eq!(result, 144);
    }
}
