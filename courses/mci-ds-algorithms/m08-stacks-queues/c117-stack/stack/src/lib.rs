pub struct Stack {
    top: Link,
}

pub struct Node {
    n: u32,
    next: Link,
}

type Link = Option<Box<Node>>;

impl Stack {
    pub fn new() -> Self {
        Stack { top: None }
    }

    pub fn push(&mut self, n: u32) {
        let old_top = self.top.take();
        let new_top = Some(Box::new(Node { n, next: old_top }));
        self.top = new_top;
    }

    pub fn pop(&mut self) -> Option<u32> {
        let old_top = self.top.take();
        let v = old_top.as_ref().map(|ot| ot.n);
        self.top = match old_top {
            Some(ot) => ot.next,
            None => None,
        };
        v
    }

    pub fn peek(&self) -> Option<u32> {
        self.top.as_ref().map(|t| t.n)
    }
}

#[cfg(test)]
mod tests {
    use crate::{Node, Stack};

    fn stack() -> Stack {
        Stack::new()
    }

    #[test]
    fn push() {
        let mut s = stack();
        s.push(10);
        assert_eq!(s.top.unwrap().n, 10);
    }

    #[test]
    fn pop() {
        let mut s = Stack {
            top: Some(Box::new(Node { n: 10, next: None })),
        };
        assert_eq!(s.pop(), Some(10));
        assert_eq!(s.pop(), None);
        assert!(s.top.is_none());
    }

    #[test]
    fn peek() {
        let mut s = stack();
        assert_eq!(s.peek(), None);
        s.push(10);
        assert_eq!(s.peek(), Some(10));
    }
}
