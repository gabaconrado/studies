pub struct MyQueue {
    stack_1: Vec<i32>,
    stack_2: Vec<i32>,
}

/**
 * `&self` means the method takes an immutable reference.
 * If you need a mutable reference, change it to `&mut self` instead.
 */
impl MyQueue {
    pub fn new() -> Self {
        Self {
            stack_1: Vec::new(),
            stack_2: Vec::new(),
        }
    }

    pub fn push(&mut self, x: i32) {
        self.move_s2_to_s1();
        self.stack_1.push(x)
    }

    pub fn pop(&mut self) -> Option<i32> {
        self.move_s1_to_s2();
        self.stack_2.pop()
    }

    pub fn peek(&mut self) -> Option<i32> {
        self.move_s1_to_s2();
        if self.stack_2.is_empty() {
            return None;
        }
        Some(self.stack_2[self.stack_2.len() - 1])
    }

    pub fn empty(&self) -> bool {
        (self.stack_1.len() + self.stack_2.len()) == 0
    }

    fn move_s1_to_s2(&mut self) {
        for _ in 0..self.stack_1.len() {
            let i = self.stack_1.pop().unwrap();
            self.stack_2.push(i);
        }
    }

    fn move_s2_to_s1(&mut self) {
        for _ in 0..self.stack_2.len() {
            let i = self.stack_2.pop().unwrap();
            self.stack_1.push(i);
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn queue() {
        let mut q = MyQueue::new();
        q.push(2);
        q.push(3);
        assert_eq!(q.peek(), Some(2));
        assert_eq!(q.pop(), Some(2));
        assert!(!q.empty());
        assert_eq!(q.pop(), Some(3));
        assert!(q.empty());
        assert_eq!(q.pop(), None);
        assert_eq!(q.peek(), None);
    }
}
