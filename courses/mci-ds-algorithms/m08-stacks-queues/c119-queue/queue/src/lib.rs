const INITIAL_SIZE: usize = 4;

pub struct Queue {
    first: usize,
    last: usize,
    length: usize,
    array: Box<[u32]>,
}

impl Queue {
    pub fn new() -> Self {
        Queue {
            first: 0,
            last: 0,
            length: 0,
            array: Box::new([0; INITIAL_SIZE]),
        }
    }

    pub fn enqueue(&mut self, n: u32) {
        if self.length >= INITIAL_SIZE {
            panic!("Queue is full");
        }
        let index = self.last % INITIAL_SIZE;
        self.array[index] = n;
        self.last += 1;
        self.length += 1;
    }

    pub fn dequeue(&mut self) -> Option<u32> {
        if self.length <= 0 {
            return None;
        }
        let index = self.first % INITIAL_SIZE;
        let v = self.array[index];
        self.first += 1;
        self.length -= 1;
        return Some(v);
    }

    pub fn peek(&self) -> Option<u32> {
        if self.length <= 0 {
            return None;
        }
        let index = self.first % INITIAL_SIZE;
        let v = self.array[index];
        Some(v)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn enqueue() {
        let mut q = Queue::new();
        q.enqueue(10);
        assert_eq!(q.array[0], 10);
        assert_eq!(q.length, 1);
    }

    #[test]
    fn dequeue() {
        let mut q = Queue::new();
        q.array[0] = 10;
        q.length = 1;
        assert_eq!(q.dequeue(), Some(10));
        assert!(q.dequeue().is_none());
        assert_eq!(q.length, 0);
    }

    #[test]
    fn peek() {
        let mut q = Queue::new();
        assert!(q.peek().is_none());
        q.array[0] = 10;
        q.length = 1;
        assert_eq!(q.peek(), Some(10));
    }

    #[test]
    fn circular_consistency() {
        let mut q = Queue::new();
        q.enqueue(1);
        q.enqueue(2);
        q.enqueue(3);
        q.enqueue(4);
        assert_eq!(*q.array, [1, 2, 3, 4]);
        assert_eq!(q.peek(), Some(1));
        assert_eq!(q.dequeue(), Some(1));
        q.enqueue(5);
        assert_eq!(*q.array, [5, 2, 3, 4]);
        assert_eq!(q.peek(), Some(2));
        assert_eq!(q.dequeue(), Some(2));
        assert_eq!(q.length, 3);
    }
}
