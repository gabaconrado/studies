/// Use the Selection Sort to sort a collection in-place
///
/// Selection sort traverses the collection N times, at every iteration it finds the lowest element
/// of the collection and puts it into the beginning
///
/// ## Example
///
/// ```rust
/// use sorters;
/// let mut a = [10, 20, 5, 6] ;
///
/// sorters::selection_sort(&mut a);
///
/// // What happens internally
/// // [10, 20, 5, 6] ->
/// // [5, 10, 20, 6] ->
/// // [5, 6, 10, 20] ->
/// // [5, 6, 10, 20] ok
/// ```
pub fn selection_sort<T>(collection: &mut [T])
where
    T: std::cmp::Ord + Copy,
{
    let n = collection.len();
    for i in 0..n {
        // Reset smallest index
        let mut smallest_index = i;
        // Find the smallest number index in the collection
        for y in i..n {
            if collection[y] < collection[smallest_index] {
                smallest_index = y;
            }
        }
        // Swaps it with the start of the collection
        let aux = collection[smallest_index];
        collection[smallest_index] = collection[i];
        collection[i] = aux;
    }
}
