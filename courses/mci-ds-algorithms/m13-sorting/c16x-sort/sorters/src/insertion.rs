/// Use the Insertion Sort to sort a collection in-space
///
/// Selection sort traverses the collection once, putting every number in its correct place already.
///
/// ## Example
///
/// ```rust
/// use sorters;
/// let mut a = [10, 20, 5, 6] ;
///
/// sorters::insertion_sort(&mut a);
///
/// // What happens internally
/// // [10, 20, 5, 6] ->
/// // [5, 10, 20, 6] ->
/// // [5, 6, 10, 20] ok
/// ```
pub fn insertion_sort<T>(collection: &mut [T])
where
    T: std::cmp::Ord + Copy,
{
    // Check if collection is already sorted
    for i in 1..collection.len() {
        // If not sorted, save the value and the index and go back to the start of the collection
        // to search for the insertion point of this value
        if collection[i] < collection[i - 1] {
            for j in 0..i {
                // Find the insertion point
                if collection[i] < collection[j] {
                    // Shift all values from insertion point up to where the element was found
                    let mut aux = collection[j];
                    collection[j] = collection[i];
                    for l in (j + 1)..i {
                        if aux < collection[l] {
                            let aux2 = collection[l];
                            collection[l] = aux;
                            aux = aux2;
                        }
                    }
                    collection[i] = aux;
                    // End this iteration
                    break;
                }
            }
        }
    }
}
