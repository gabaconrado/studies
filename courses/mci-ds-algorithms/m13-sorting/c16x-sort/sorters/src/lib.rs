#![feature(test)]
//! # Sorters
//!
//! Diverse sorting algorithm implementations.
//!
//! Learning purposes only.

/// Bubble sort implementation
mod bubble;
/// Insertion sort implementation
mod insertion;
/// Merge sort implementation
mod merge;
/// Quick sort implementation
mod quick;
/// Selection sort implementation
mod selection;

pub use self::{
    bubble::bubble_sort, insertion::insertion_sort, merge::merge_sort, quick::quick_sort,
    selection::selection_sort,
};

#[cfg(test)]
mod tests {
    use crate::quick::quick_sort;

    use super::*;

    #[test]
    fn should_sort_using_bubble() {
        let mut collection = [10, 2, 66, 87, 5, 2, -4];
        let expected = [-4, 2, 2, 5, 10, 66, 87];

        bubble_sort(&mut collection);

        assert_eq!(collection, expected);
    }

    #[test]
    fn should_sort_using_selection() {
        let mut collection = [10, 2, 66, 87, 5, 2, -4];
        let expected = [-4, 2, 2, 5, 10, 66, 87];

        selection_sort(&mut collection);

        assert_eq!(collection, expected);
    }

    #[test]
    fn should_sort_using_insertion() {
        let mut collection = [10, 2, 66, 87, 5, 2, -4];
        let expected = [-4, 2, 2, 5, 10, 66, 87];

        insertion_sort(&mut collection);

        assert_eq!(collection, expected);
    }

    #[test]
    fn should_sort_using_merge() {
        let collection = [10, 2, 66, 87, 5, 2, -4];
        let expected = [-4, 2, 2, 5, 10, 66, 87];

        let sorted = merge_sort(&collection);

        assert_eq!(sorted, expected);
    }

    #[test]
    fn should_sort_using_quick() {
        let collection = [10, 2, 66, 87, 5, 2, -4].to_vec();
        let expected = [-4, 2, 2, 5, 10, 66, 87];

        let sorted = quick_sort(collection);

        assert_eq!(sorted, expected);
    }
}

/// Benchmarks
#[cfg(test)]
mod benches;
