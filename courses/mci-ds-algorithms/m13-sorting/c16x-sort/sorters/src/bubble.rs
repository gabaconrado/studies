/// Use the Bubble Sort algorithm to sort a collection in-place
///
/// Bubble sort goes index by index, at every iteration it finds the biggest element of the vector
/// and sends it to the back of the collection.
///
/// ## Example
///
/// ```rust
/// use sorters;
///
/// let mut a = [10, 20, 5, 6];
/// sorters::bubble_sort(&mut a);
///
/// // What happens internally:
/// // [10, 20, 5, 6] ->
/// // [10, 5, 6, 20] ->
/// // [5, 6, 10, 20] ->
/// // [5, 6, 10, 20] ok
/// ```
pub fn bubble_sort<T>(collection: &mut [T])
where
    T: std::cmp::Ord + Copy,
{
    let n = collection.len();
    for _ in 0..n {
        for y in 0..n - 1 {
            if collection[y] > collection[y + 1] {
                let aux = collection[y + 1];
                collection[y + 1] = collection[y];
                collection[y] = aux;
            }
        }
    }
}
