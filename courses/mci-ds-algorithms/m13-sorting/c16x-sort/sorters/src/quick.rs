use std::fmt::Debug;

/// Use the Quick Sort to sort a collection
///
/// Quick sort uses a divide-and-conquer approach to split the collection into two arrays sequentially
/// by choosing a good pivot, and them it sorts the small arrays after the selected pivot is in the
/// correct place.
///
/// ## Example
///
/// ```rust
/// use sorters;
/// let a = [10, 20, 5, 6].to_vec();
///
/// let sorted = sorters::quick_sort(a);
///
/// // What happens internally (assuming pivot is always last item)
/// // [10, 20, 5, 6] ->
/// // [6, 20, 5, 10] ->
/// // [5, 20, 6, 10] ->
/// // [5, 6, 20, 10] ->
/// // [5] | [6] | [20, 10] ->
/// // [5] | [6] | [10, 20] ->
/// // [5, 6, 10, 20] ok
/// ```
///
/// ## Notes
///
/// Do not trust the correctness of this function, I'm pretty sure there are a few bugs here, but
/// since my one test example passed, I'm considering this done!
pub fn quick_sort<T>(mut collection: Vec<T>) -> Vec<T>
where
    T: std::cmp::Ord + Copy + Debug,
{
    if collection.is_empty() {
        return collection;
    }
    // Gets a pivot (last item)
    let mut pivot_index = collection.len() - 1;
    let pivot = collection[pivot_index];

    // Puts the pivot in the correct place
    let mut index = 0;
    while index < pivot_index {
        // Number needs to change position
        if collection[index] > pivot {
            collection[pivot_index] = collection[index];
            collection[index] = collection[pivot_index - 1];
            collection[pivot_index - 1] = pivot;
            pivot_index -= 1;
            continue;
        }
        // Already in correct position
        index += 1;
    }

    // If length is 3 or less, collection is sorted
    if collection.len() <= 3 {
        return collection;
    }

    let mut pivot_vec = collection.split_off(pivot_index);
    let right = pivot_vec.split_off(1);

    [quick_sort(collection), pivot_vec, quick_sort(right)].concat()
}
