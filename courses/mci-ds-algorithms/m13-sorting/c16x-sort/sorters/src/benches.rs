use rand::{distributions::Standard, prelude::Distribution, thread_rng, Rng};

extern crate test;

fn create_array<T>(n: usize) -> Vec<T>
where
    T: Ord,
    Standard: Distribution<T>,
{
    let mut rng = thread_rng();
    (0..n).map(|_| rng.gen()).collect()
}

mod sorting {
    use super::*;
    use test::Bencher;

    #[bench]
    fn sort_random_1000_array_bubble(b: &mut Bencher) {
        let n = test::black_box(1000);
        let mut array = test::black_box(create_array::<u32>(n));

        b.iter(|| crate::bubble_sort(&mut array))
    }

    #[bench]
    fn sort_random_1000_array_selection(b: &mut Bencher) {
        let n = test::black_box(1000);
        let mut array = test::black_box(create_array::<u32>(n));

        b.iter(|| crate::selection_sort(&mut array))
    }

    #[bench]
    fn sort_random_1000_array_insertion(b: &mut Bencher) {
        let n = test::black_box(1000);
        let mut array = test::black_box(create_array::<u32>(n));

        b.iter(|| crate::insertion_sort(&mut array))
    }

    #[bench]
    fn sort_random_1000_array_merge(b: &mut Bencher) {
        let n = test::black_box(1000);
        let array = test::black_box(create_array::<u32>(n));

        b.iter(|| crate::merge_sort(&array));
    }

    #[bench]
    fn sort_random_1000_array_quick(b: &mut Bencher) {
        let n = test::black_box(1000);
        let array = test::black_box(create_array::<u32>(n));

        b.iter(|| crate::quick_sort(array.clone()))
    }
}
