/// Use the Merge Sort to sort a collection
///
/// Merge sort uses a divide-and-conquer approach to split the collection into individual arrays,
/// and then sequentially sort two-by-two until the original vector is sorted.
///
/// ## Example
///
/// ```rust
/// use sorters;
/// let mut a = [10, 20, 5, 6] ;
///
/// let sorted = sorters::merge_sort(&a);
///
/// // What happens internally
/// // [10, 20, 5, 6] ->
/// // [10, 20] | [5, 6] ->
/// // [10] | [20] | [5] | [6] ->
/// // [10, 20] | [5, 6] ->
/// // [5, 6, 10, 20] ok
/// ```
pub fn merge_sort<T>(collection: &[T]) -> Vec<T>
where
    T: std::cmp::Ord + Copy,
{
    let size = collection.len();
    if size == 1 {
        return collection.to_vec();
    }

    let (left, right) = collection.split_at(size / 2);

    return merge(merge_sort(left), merge_sort(right));
}

/// Merges two unsorted vectors into a sorted one
fn merge<T>(mut left: Vec<T>, mut right: Vec<T>) -> Vec<T>
where
    T: std::cmp::Ord + Copy,
{
    let mut result = Vec::with_capacity(left.len() + right.len());
    loop {
        if left.is_empty() && right.is_empty() {
            return result;
        }
        if left.is_empty() {
            result.push(right.remove(0));
        } else if right.is_empty() {
            result.push(left.remove(0));
        } else {
            if left[0] <= right[0] {
                result.push(left.remove(0));
            } else {
                result.push(right.remove(0));
            }
        }
    }
}
