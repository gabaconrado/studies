pub fn merge_sorted_arrays(array1: &[u32], array2: &[u32]) -> Vec<u32> {
    let w = array1.len() + array2.len();
    let mut y = 0;
    let mut z = 0;
    let mut output = Vec::<u32>::with_capacity(w);
    for _ in 0..w {
        let should_copy_array1 = z >= array2.len() || array1[y] <= array2[z];
        if should_copy_array1 {
            output.push(array1[y]);
            y += 1;
        } else {
            output.push(array2[z]);
            z += 1;
        }
    }
    output
}

fn main() {
    println!("Hello, world!");
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn should_merge_arrays() {
        let array1 = [0, 3, 4, 31];
        let array2 = [4, 6, 8, 10, 30];
        let merged_array = merge_sorted_arrays(&array1, &array2);
        assert_eq!(merged_array, [0, 3, 4, 4, 6, 8, 10, 30, 31]);
    }
}
