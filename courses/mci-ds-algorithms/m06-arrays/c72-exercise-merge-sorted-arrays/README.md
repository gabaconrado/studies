# Exercise

Given two sorted arrays, merge them into a single merged array.

## Examples

```rust
let array1 = [0, 3, 4, 31];
let array2 = [4, 6, 30];
let merged_array = merge_sorted_arrays(&array1, &array2);
assert_eq!(merged_array, [0, 3, 4, 4, 6, 30, 31]);
```
