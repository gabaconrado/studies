fn reverse_string(mut input: String) -> String {
    // Constraint: Input is ASCII
    // Simplest solution:
    // Create another string, iterate through input[len-1] -> input[0] copying each char
    // O(n) speed, O(n) space
    // A better solution:
    // In original string, use auxiliar var to replace input[len-1]  with input[0],
    // then input[len -2] with input[1], and so on.
    // Examples: Even length
    // abcdef -> fbcdea -> fecdba -> fedcba (3 iterations = n / 2)
    // Examples: Odd length
    // abcde -> ebcda -> edcba (2 iterations = n div 2)
    // O(n div 2) = O(n) speed, O(1) space, since it is in-place
    let mut y = input.len() - 1;
    let mut i = 0;
    // As bytes mut is unsafe, he-he
    unsafe {
        let x = input.as_bytes_mut();
        while i < y {
            let aux = x[i];
            x[i] = x[y];
            x[y] = aux;
            i = i + 1;
            y = y - 1;
        }
    }
    input
}

fn main() {
    let input = "Hello, world!".to_string();
    let reversed = reverse_string(input.clone());
    println!("{} -> {}", input, reversed);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn should_revert_string() {
        let input = "Hello, world!".to_string();
        let reversed = reverse_string(input);
        assert_eq!(reversed, "!dlrow ,olleH");
    }
}
