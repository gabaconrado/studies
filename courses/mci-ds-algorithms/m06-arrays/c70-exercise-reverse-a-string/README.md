# Exercise

Given a string of length N, create a function to revert it:

## Examples

```rust
let string = "This is a string";
let reversed = revertString(string);
assert_eq!(reversed, "gnirts a si sihT");
```
