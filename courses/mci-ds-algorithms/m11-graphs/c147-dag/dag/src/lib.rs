use std::collections::HashMap;

type Node = u32;

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Graph {
    size: u32,
    adjacency_list: HashMap<Node, Vec<Node>>,
}

impl Graph {
    pub fn new() -> Self {
        Self {
            size: 0,
            adjacency_list: HashMap::new(),
        }
    }

    pub fn add_vertex(&mut self, node: Node) {
        self.adjacency_list.insert(node, vec![]);
        self.size += 1;
    }

    pub fn add_edge(&mut self, from: Node, to: Node) {
        match self.adjacency_list.get_mut(&from) {
            Some(n) => n.push(to),
            None => {}
        }
        match self.adjacency_list.get_mut(&to) {
            Some(n) => n.push(from),
            None => {}
        }
    }

    pub fn len(&self) -> u32 {
        self.size
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn new() {
        let graph = Graph::new();
        assert_eq!(graph.size, 0);
        assert!(graph.adjacency_list.is_empty());
    }

    #[test]
    fn add_vertex() {
        let mut g = empty_graph();
        let node = 10;
        let prev_size = g.size;

        g.add_vertex(node);
        assert_eq!(g.size, prev_size + 1);
        assert!(g.adjacency_list.contains_key(&node));
    }

    #[test]
    fn add_edge() {
        let mut g = populated_graph();
        let from = 0;
        let to = 6;

        g.add_edge(from, to);
        assert!(g.adjacency_list.get(&from).unwrap().contains(&to));
    }

    #[test]
    fn length() {}

    // Fixtures

    fn populated_graph() -> Graph {
        let size = 6;
        let adjacency_list = [
            (0, vec![1, 2]),
            (1, vec![0, 2, 3]),
            (3, vec![1, 4]),
            (4, vec![2, 3, 5]),
            (5, vec![4, 6]),
            (6, vec![5]),
        ];
        Graph {
            size,
            adjacency_list: adjacency_list.into(),
        }
    }

    fn empty_graph() -> Graph {
        Graph::new()
    }
}
