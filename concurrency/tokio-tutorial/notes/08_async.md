# Async in depth

## Futures

- A `Future` does not represent a computation happening in the background;
- The `Future` is the computation itself, which means the owner is responsible for advancing or waiting in its results;
- `Future`s are **state machines**;
- `Future`s usually are a composition of other `Future`s;

## Wakers

- The executors will usually only call `poll` when the future is ready to make more progress;
- This means that if the `Future` implementation forgets to setup the `waker`, the task will hang indefinitely;
- There is nothing wrong in signalizing the waker more than is needed, this is just a waste of CPU cycles;
- `tokio::sync::Notify` can be used to abstract away Wakers in simple setups;
