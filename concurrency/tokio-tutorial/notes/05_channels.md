# Channels

## Summary

- In the client side, we may want to run multiple tasks concurrently;
- To achieve that, using mutexes or opening multiple connections are suboptimal;
- The idea is to have a dedicated tasks to handle client operations;
- Other tasks would be able to send messages to that dedicated task via `Channels`;

## Tokyo channel types

- `mpsc`: multi producer, single consumer;
- `oneshot`: single producer, single consumer;
- `broadcast`: multi producer, multi consumer;
- `watch`: single producer, multi consumer;

## Sending the command

- To send the commands to the client task, a `mpsc` channel is used;

## Receiving response

- To receive the result of the command, the client task will use an `oneshot` channel;

## Bounds in channels

- Always put reasonable bounds in the objects to avoid capacity/performance problems;
