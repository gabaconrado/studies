# Shared State

## Strategies

- Two ways of dealing with shared state:
  - Guard the shared state with `Arc<Mutex<T>>`;
  - Dedicated task to perform state operations;
- The first solution is usually simpler;
- The second solution is used when state operations are async;

## Use `std::sync::Mutex` or `tokio::sync::Mutex`?

- Unconditional usage of `tokio::sync::Mutex` is not the correct way;
- Tokio mutexes are useful when you need to hold locks across await calls;
- In common and simple cases, commons mutexes will do the job;
- Obs.: `parking_lot::Mutex` is a faster alternative to `std::sync::Mutex`;

## Tasks, threads and contention

- Blocking mutexes are acceptable when contention is minimum;
- `current_thread` runtime flavor is a lightweight, single-threaded runtime that can minimize contention;
- If contention becomes a problem, changing to `tokio::sync::Mutex` is probably not the best solution;
  - Switch to dedicated task to manage the state can be better, or;
  - Shard the mutex, or;
  - Restructure the code;
