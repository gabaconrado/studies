# Spawning

## Accepting sockets

- We use the tokio TCP directives to bind to a socket and accept incoming connections;
- Just echo the message that we receive;

## Concurrency

- Noteworthy that we will implement **concurrency** and not **parallelism**;
- Tokio tasks are _asynchronous green threads_;
- Tasks are the unit of work, and they can be spawned to perform work asynchronously (very cheap)!

## Bounds

- All tasks must receive functions that are `'static` and `SEND`, so the compiler can ensure they can outlive the current function and they can switch threads;
- We can use the `move` keyword to move values into the async blocks;
