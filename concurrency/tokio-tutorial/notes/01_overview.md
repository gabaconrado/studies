# Overview

## Characteristics

- Tokio is the most used async runtime for Rust;
- Implements its own async components of the standard library;
- Fast, reliable, flexible and easy;

## When not to use Tokio

- When the software needs to speed CPU-bound tasks;
- When the software needs to read a lot of files;
- When the software needs to perform tasks individually and not a lot of tasks at the same time;

## TODO

- [ ] Follow the tutorial to build `mini-redis`
  - [ ] TODO: Implement client set/get example
