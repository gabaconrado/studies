# Setup

Mini Redis set/get simple example

## Code

```rust
use mini_redis::{client, Result};

#[tokio::main]
async fn main() -> Result<()> {
    // Open a connection to the mini-redis address.
    let mut client = client::connect("127.0.0.1:6379").await?;

    // Set the key "hello" with value "world"
    client.set("hello", "world".into()).await?;

    // Get key "hello"
    let result = client.get("hello").await?;

    println!("got value from the server; result={:?}", result);

    Ok(())
}
```

## Breaking down

### Async programming

- Asynchronous programming means that the program is not necessarily executed in the same order as the commands are defined;
- When the software needs to wait for an instruction, it will yield control of the thread so it can perform other tasks, instead of blocking;
- Asynchronous programming is historically more complicated and error prone than basic synchronous programming;

### Compile-time green threading

- Rust provides `async/await` directives to write asynchronous code that looks a lot like synchronous code;
- It is noteworthy that the constraints are very different, though;
- All `async/await` code is **lazy** in Rust;
- The return value of an `async` function is a `Future<T>`;
