# Framing

- Parse a byte stream and transform it into a frame stream.

## Buffered reads

- The TCP stream will be a sequence of bytes;
- It is necessary to split these bytes into logical frames, and return exactly one full frame in each frame call;
