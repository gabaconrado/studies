#[derive(Clone, Debug)]
pub enum Command {
    Get { key: String },
    Set { key: String, val: String },
}

impl From<&Command> for Vec<u8> {
    fn from(cmd: &Command) -> Self {
        let cmd_str = match cmd {
            Command::Get { key } => format!("get|{}", key.clone()),
            Command::Set { key, val } => format!("set|{}|{}", key.clone(), val.clone()),
        };
        cmd_str.as_bytes().to_vec()
    }
}

impl From<&[u8]> for Command {
    fn from(cmd_bytes: &[u8]) -> Self {
        let cmd_str = String::from_utf8(cmd_bytes.to_vec()).unwrap();
        let args = cmd_str.split("|").collect::<Vec<&str>>();
        match args[0] {
            "get" => Command::Get {
                key: args[1].to_string(),
            },
            "set" => Command::Set {
                key: args[1].to_string(),
                val: args[2].to_string(),
            },
            cmd => panic!("Weird command: {}", cmd),
        }
    }
}
