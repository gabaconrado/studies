use mini_redis::Command;
use tokio::{
    io::{AsyncReadExt, AsyncWriteExt},
    net::TcpSocket,
    sync::{mpsc, oneshot},
};

type Responder = oneshot::Sender<String>;

#[derive(Debug)]
struct Cmd {
    cmd: Command,
    resp: Responder,
}

impl From<&Cmd> for Command {
    fn from(cmd: &Cmd) -> Self {
        cmd.cmd.clone()
    }
}

#[tokio::main]
async fn main() {
    println!("Starting client");
    let (tx, mut rx) = mpsc::channel(32);
    let addr = "127.0.0.1:6379".parse().unwrap();

    let t2 = tx.clone();
    tokio::spawn(async move {
        let (t1tx, t1rx) = oneshot::channel();
        let cmd1 = Cmd {
            cmd: Command::Set {
                key: "getkey".to_string(),
                val: "getkey".to_string(),
            },
            resp: t1tx,
        };

        t2.send(cmd1).await.unwrap();
        let res = t1rx.await.unwrap();
        println!("Response: {}", res);
    });

    tokio::spawn(async move {
        let (t1tx, t1rx) = oneshot::channel();
        let cmd1 = Cmd {
            cmd: Command::Get {
                key: "getkey".to_string(),
            },
            resp: t1tx,
        };

        tx.send(cmd1).await.unwrap();
        let res = t1rx.await.unwrap();
        println!("Response: {}", res);
    });

    while let Some(cmd) = rx.recv().await {
        let mut buf = [0; 1024];
        let mut socket = TcpSocket::new_v4().unwrap().connect(addr).await.unwrap();
        let c = Command::from(&cmd);
        let bytes: Vec<u8> = (&c).into();
        socket.write(&bytes).await.unwrap();

        let len = socket.read(&mut buf).await.unwrap();
        let response = &buf[..len];
        cmd.resp
            .send(String::from_utf8_lossy(response).to_string())
            .unwrap();
    }
}
