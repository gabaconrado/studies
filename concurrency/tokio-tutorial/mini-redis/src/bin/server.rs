//! Mini-redis server implementation

use mini_redis::Command;
use std::collections::HashMap;
use std::sync::{Arc, Mutex};
use tokio::io::AsyncWriteExt;

use tokio::{
    io::AsyncReadExt,
    net::{TcpListener, TcpStream},
};

type Db = Arc<Mutex<HashMap<String, String>>>;

#[tokio::main]
async fn main() {
    // Bind to address
    let listener = TcpListener::bind("127.0.0.1:6379").await.unwrap();
    println!("Started server");

    let db = Arc::new(Mutex::new(HashMap::new()));

    loop {
        let (socket, _) = listener.accept().await.unwrap();
        let db = db.clone();
        tokio::spawn(async move {
            process(socket, db).await;
        });
    }
}

async fn process(mut socket: TcpStream, mut database: Db) {
    let mut buf = [0; 1024];

    // Read from the socket
    let len = socket.read(&mut buf).await.unwrap();
    let cmd = Command::from(&buf[..len]);
    let response = process_command(&cmd, &mut database);
    socket.write(response.as_bytes()).await.unwrap();
}

fn process_command(cmd: &Command, database: &mut Db) -> String {
    match cmd {
        Command::Get { key } => database
            .lock()
            .unwrap()
            .get(key)
            .unwrap_or(&"Key does not exist".to_string())
            .to_string(),
        Command::Set { key, val } => {
            database
                .lock()
                .unwrap()
                .insert(key.to_string(), val.to_string());
            "Key inserted".to_string()
        }
    }
}
